package gorminit

import (
	"errors"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	gormLog "gorm.io/gorm/logger"
	"gorm.io/plugin/dbresolver"
	"strings"
	"time"
	"yooome/global/syserrors"
	"yooome/global/variable"
)

// 获取一个 mysql 客户端
func InitMysqlConnect() (*gorm.DB, error) {
	sqlType := "Mysql"
	readDBIsOpen := variable.ConfigGormYml.GetInt("GormMysql." + sqlType + "IsOpenReadDb")
	return MysqlDriver(sqlType, readDBIsOpen)

}
func MysqlDriver(sqlType string, readDBIsOpen int, dbConfig ...ConfigParams) (*gorm.DB, error) {
	var dial gorm.Dialector
	val, err := initDialector(sqlType, "Write", dbConfig...)
	if err != nil {
		variable.Logrus.Error(syserrors.ErrorsDialectorDbInitFail+sqlType, err)
	} else {
		dial = val
	}
	gormDB ,err := gorm.Open(dial, &gorm.Config{
		SkipDefaultTransaction: true,
		PrepareStmt:            true,
		Logger:                 redefineLog(sqlType), //拦截、接管 gorm v2 自带日志
	})
	if err != nil {
		// gorm初始化驱动失败
		return nil,err
	}
	// 如果开启了读写分离，配置读数据库（resource，read，replicas）
	// 读写分离配置
	if readDBIsOpen == 1 {
		if val,err := initDialector(sqlType,"Read",dbConfig...);err != nil {
			fmt.Println("readDBIsOpen  val ... ")
		}else {
			dial = val
		}
		resolverConf := dbresolver.Config{
			Replicas: []gorm.Dialector{dial}, //  读 操作库，查询类
			Policy:   dbresolver.RandomPolicy{},     // sources/replicas 负载均衡策略适用于
		}
		err = gormDB.Use(dbresolver.Register(resolverConf).SetConnMaxIdleTime(time.Second * 30).
			SetConnMaxLifetime(variable.ConfigGormYml.GetDuration("GormMysql."+sqlType+".Read.SetConnMaxLifetime") * time.Second).
			SetMaxIdleConns(variable.ConfigGormYml.GetInt("GormMysql." + sqlType + ".Read.SetMaxIdleConns")).
			SetMaxOpenConns(variable.ConfigGormYml.GetInt("GormMysql." + sqlType + ".Read.SetMaxOpenConns")))
		if err != nil {
			return nil, err
		}
	}
	// 查询没有数据，屏蔽 gorm v2 包中会爆出的错误
	// https://github.com/go-gorm/gorm/issues/3789  此 issue 所反映的问题就是我们本次解决掉的
	_ = gormDB.Callback().Query().Before("gorm:query").Register("disable_raise_record_not_found", func(d *gorm.DB) {
		d.Statement.RaiseErrorOnNotFound = false
	})
	// 为主连接设置连接池(43行返回的数据库驱动指针)
	if rawDb, err := gormDB.DB(); err != nil {
		return nil, err
	} else {
		rawDb.SetConnMaxIdleTime(time.Second * 30)
		rawDb.SetConnMaxLifetime(variable.ConfigGormYml.GetDuration("GormMysql."+sqlType+".Write.SetConnMaxLifetime") * time.Second)
		rawDb.SetMaxIdleConns(variable.ConfigGormYml.GetInt("GormMysql." + sqlType + ".Write.SetMaxIdleConns"))
		rawDb.SetMaxOpenConns(variable.ConfigGormYml.GetInt("GormMysql." + sqlType + ".Write.SetMaxOpenConns"))
		return gormDB, nil
	}
}

func initDialector(sqlType string, readWrite string, dbConf ...ConfigParams) (gorm.Dialector, error) {
	var dial gorm.Dialector
	dsn := initDsn(sqlType, readWrite, dbConf...)
	switch strings.ToLower(sqlType) {
	case "mysql":
		dial = mysql.Open(dsn)
	default:
		return nil, errors.New(syserrors.ErrorsDbDriverNotExists + sqlType)
	}
	return dial, nil
}

// 根据配置参数生成数据库驱动 dsn
func initDsn(sqlType string, readWrite string, dbConf ...ConfigParams) string {
	Host := variable.ConfigGormYml.GetString("GormMysql." + sqlType + "." + readWrite + ".Host")
	DataBase := variable.ConfigGormYml.GetString("GormMysql." + sqlType + "." + readWrite + ".DataBase")
	Port := variable.ConfigGormYml.GetInt("GormMysql." + sqlType + "." + readWrite + ".Port")
	User := variable.ConfigGormYml.GetString("GormMysql." + sqlType + "." + readWrite + ".User")
	Pass := variable.ConfigGormYml.GetString("GormMysql." + sqlType + "." + readWrite + ".Pass")
	Charset := variable.ConfigGormYml.GetString("GormMysql." + sqlType + "." + readWrite + ".Charset")

	if len(dbConf) > 0 {
		if strings.ToLower(readWrite) == "write" {
			if len(dbConf[0].Write.Host) > 0 {
				Host = dbConf[0].Write.Host
			}
			if len(dbConf[0].Write.DataBase) > 0 {
				Host = dbConf[0].Write.Host
			}
			if dbConf[0].Write.Port > 0 {
				Host = dbConf[0].Write.Host
			}
			if len(dbConf[0].Write.User) > 0 {
				Host = dbConf[0].Write.User
			}
			if len(dbConf[0].Write.Pass) > 0 {
				Host = dbConf[0].Write.Pass
			}
			if len(dbConf[0].Write.Charset) > 0 {
				Host = dbConf[0].Write.Charset
			}
		} else {
			if len(dbConf[0].Read.Host) > 0 {
				Host = dbConf[0].Read.Host
			}
			if len(dbConf[0].Read.DataBase) > 0 {
				Host = dbConf[0].Read.Host
			}
			if dbConf[0].Read.Port > 0 {
				Host = dbConf[0].Read.Host
			}
			if len(dbConf[0].Read.User) > 0 {
				Host = dbConf[0].Read.User
			}
			if len(dbConf[0].Read.Pass) > 0 {
				Host = dbConf[0].Read.Pass
			}
			if len(dbConf[0].Read.Charset) > 0 {
				Host = dbConf[0].Read.Charset
			}
		}
	}
	switch strings.ToLower(sqlType) {
	case "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=True&loc=Local", User, Pass, Host, Port, DataBase, Charset)
	}
	return ""
}

func redefineLog(sqlType string) gormLog.Interface {
	return initCustomGormLog(sqlType,
		SetInfoStrFormat("[info] %s\n"),
		SetWarnStrFormat("[warn] %s\n"),
		SetErrStrFormat("[error] %s\n"),
		SetTraceStrFormat("[traceStr] %s [%.3fms] [rows:%v] %s\n"),
		SetTracWarnStrFormat("[traceWarn] %s %s [%.3fms] [rows:%v] %s\n"),
		SetTracErrStrFormat("[traceErr] %s %s [%.3fms] [rows:%v] %s\n"))
}
