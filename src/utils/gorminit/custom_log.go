package gorminit

import (
	"context"
	"fmt"
	gormLog "gorm.io/gorm/logger"
	"strings"
	"time"
	"yooome/global/variable"
)

type Options interface {
	apply(*logger)
}
type OptionFunc func(log *logger)

func (f OptionFunc) apply(log *logger) {
	f(log)
}

type logOutPut struct{}

// 自定义日志格式, 对gorm自带日志进行拦截重写
func initCustomGormLog(sqlType string, options ...Options) gormLog.Interface {
	var (
		infoStr      = "%s\n[info]"
		warnStr      = "%s\n[warn]"
		errStr       = "%s\n[error]"
		traceStr     = "%s\n[%.3fms] [rows:%v] %s"
		traceWarnStr = "%s %s\n[%.3fms] [rows:%v] %s"
		traceErrStr  = "%s %s\n[%.3fms] [rows:%v] %s"
	)
	logConf := gormLog.Config{
		SlowThreshold: time.Second * variable.ConfigGormYml.GetDuration("GormMysql."+sqlType+"."+".SlowThreshold"),
		LogLevel:      gormLog.Warn,
		Colorful:      false,
	}
	log := &logger{
		Writer:       logOutPut{},
		Config:       logConf,
		infoStr:      infoStr,
		warnStr:      warnStr,
		errStr:       errStr,
		traceStr:     traceStr,
		traceWarnStr: traceWarnStr,
		traceErrStr:  traceErrStr,
	}
	for _, val := range options {
		val.apply(log)
	}
	return log
}
func (l logOutPut) Printf(strFormat string, args ...interface{}) {
	logRes := fmt.Sprintf(strFormat, args...)
	logFlag := "gorm_v2 日志:"
	detailFlag := "详情："
	if strings.HasPrefix(strFormat, "[info]") || strings.HasPrefix(strFormat, "[traceStr]") {
		fmt.Println("logRes = ", logRes, "logFlag = ", logFlag, "detailFlag = ", detailFlag)
	} else if strings.HasPrefix(strFormat, "[error]") || strings.HasPrefix(strFormat, "[traceErr]") {
		fmt.Println("logRes = ", logRes, "logFlag = ", logFlag, "detailFlag = ", detailFlag)
	} else if strings.HasPrefix(strFormat, "[warn]") || strings.HasPrefix(strFormat, "[traceWarn]") {
		fmt.Println("logRes = ", logRes, "logFlag = ", logFlag, "detailFlag = ", detailFlag)
	}
}

// 定义6个函数修改内部函数
func SetInfoStrFormat(format string) Options {
	return OptionFunc(func(log *logger) {
		log.infoStr = format
	})
}
func SetWarnStrFormat(format string) Options {
	return OptionFunc(func(log *logger) {
		log.warnStr = format
	})
}
func SetErrStrFormat(format string) Options {
	return OptionFunc(func(log *logger) {
		log.errStr = format
	})
}
func SetTraceStrFormat(format string) Options {
	return OptionFunc(func(log *logger) {
		log.traceStr = format
	})
}
func SetTracWarnStrFormat(format string) Options {
	return OptionFunc(func(log *logger) {
		log.traceWarnStr = format
	})
}
func SetTracErrStrFormat(format string) Options {
	return OptionFunc(func(log *logger) {
		log.traceErrStr = format
	})
}

type logger struct {
	gormLog.Writer
	gormLog.Config
	infoStr      string
	warnStr      string
	errStr       string
	traceStr     string
	traceErrStr  string
	traceWarnStr string
}

func (l *logger) LogMode(level gormLog.LogLevel) gormLog.Interface {
	newlogger := *l
	newlogger.LogLevel = level
	return &newlogger
}
func (l *logger) Info(_ context.Context, msg string, data ...interface{}){
	if l.LogLevel >= gormLog.Info {
		fmt.Println("logger info ....")
	}
}
func (l *logger) Warn(_ context.Context, msg string, data ...interface{}) {
	if l.LogLevel >= gormLog.Warn {
		fmt.Print("logger warn ...")
	}
}
func (l *logger) Error(_ context.Context, msg string, data ...interface{}) {
	if l.LogLevel >= gormLog.Error {
		fmt.Print("logger error ...")
	}
}
func (l *logger) Trace(_ context.Context, begin time.Time, fc func() (string, int64), err error)  {
	fmt.Println("logger trace ...")
}
