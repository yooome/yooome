package safelist

import (
	"bytes"
	"container/list"
	"encoding/json"
	"yooome/utils/convert"
	"yooome/utils/rwmutex"
)

type SafeList struct {
	mu       rwmutex.RWMutex
	safeList *list.List
}
type Element = list.Element

func New(safe ...bool) *SafeList {
	return &SafeList{
		mu:       rwmutex.Create(safe...),
		safeList: list.New(),
	}
}

// create and returns a list from copy of given slice<array>
func NewFrom(array []interface{}, safe ...bool) *SafeList {
	l := list.New()
	for _, v := range array {
		l.PushBack(v)
	}
	return &SafeList{
		mu:       rwmutex.Create(safe...),
		safeList: l,
	}
}

func (s *SafeList) PushFront(v interface{}) (e *Element) {
	s.mu.Lock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	e = s.safeList.PushFront(v)
	s.mu.Unlock()
	return
}
func (s *SafeList) PushBack(v interface{}) (e *Element) {
	s.mu.Lock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	e = s.safeList.PushBack(v)
	s.mu.Unlock()
	return
}
func (s *SafeList) PushFronts(values []interface{}) {
	s.mu.Lock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	for _, v := range values {
		s.safeList.PushFront(v)
	}
	s.mu.Unlock()
}
func (s *SafeList) PushBacks(values []interface{}) {
	s.mu.Lock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	for _, v := range values {
		s.safeList.PushBack(v)
	}
	s.mu.Unlock()
}
func (s *SafeList) PopBack() (value interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	if e := s.safeList.Back(); e != nil {
		value = s.safeList.Remove(e)
	}
	return
}
func (s *SafeList) PopFront() (value interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	if e := s.safeList.Front(); e != nil {
		value = s.safeList.Remove(e)
	}
	return
}

func (s *SafeList) PopBacks(max int) (value []interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
		return
	}
	length := s.safeList.Len()
	if length > 0 {
		if max > 0 && max < length {
			length = max
		}
		value := make([]interface{}, length)
		for i := 0; i < length; i++ {
			value[i] = s.safeList.Remove(s.safeList.Back())
		}
	}
	return
}
func (s *SafeList) PopFronts(max int) (value []interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
		return
	}
	length := s.safeList.Len()
	if length > 0 {
		if max > 0 && max < length {
			length = max
		}
		value = make([]interface{}, length)
		for i := 0; i < length; i++ {
			value[i] = s.safeList.Remove(s.safeList.Front())
		}
	}
	return
}
func (s *SafeList) PopBackAll() []interface{} {
	return s.PopBacks(-1)
}
func (s *SafeList) PopFrontAll() []interface{} {
	return s.PopFronts(-1)
}
func (s *SafeList) FrontAll() (value []interface{}) {
	s.mu.Lock()
	defer s.mu.Lock()
	if s.safeList == nil {
		return
	}
	length := s.safeList.Len()
	if length > 0 {
		value = make([]interface{}, length)
		for i, e := 0, s.safeList.Front(); i < length; i, e = i+1, e.Next() {
			value[i] = e.Value
		}
	}
	return
}
func (s *SafeList) BackAll() (value []interface{}) {
	s.mu.Lock()
	defer s.mu.Lock()
	if s.safeList == nil {
		return
	}
	length := s.safeList.Len()
	if length > 0 {
		value = make([]interface{}, length)
		for i, e := 0, s.safeList.Back(); i < length; i, e = i+1, e.Prev() {
			value[i] = e.Value
		}
	}
	return
}
func (s *SafeList) BackValue() (value interface{}) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	if s.safeList == nil {
		return
	}
	if e := s.safeList.Back(); e != nil {
		value = e.Value
	}
	return
}
func (s *SafeList) Front() (e *Element) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	if s.safeList == nil {
		return
	}
	e = s.safeList.Front()
	return e
}
func (s *SafeList) Len() (length int) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	if s.safeList == nil {
		return
	}
	return s.safeList.Len()
}
func (s *SafeList) Size() (length int) {
	return s.Len()
}
func (s *SafeList) MoveBefore(e, p *Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		return
	}
	s.safeList.MoveBefore(e, p)
}
func (s *SafeList) MoveAfter(e, p *Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	s.safeList.MoveAfter(e, p)
}
func (s *SafeList) MoveToFront(e *Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	s.safeList.MoveToFront(e)
}
func (s *SafeList) MoveToBack(e *Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	s.safeList.MoveToBack(e)
}
func (s *SafeList) PushBackList(e *SafeList) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	s.safeList.PushBackList(e.safeList)
}

func (s *SafeList) PushFrontList(e *SafeList) {
	if s != e {
		e.mu.Lock()
		defer e.mu.Unlock()
	}
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	s.safeList.PushFrontList(e.safeList)
}
func (s *SafeList) InsertAfter(p *Element, v interface{}) (e *Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	e = s.safeList.InsertAfter(v, p)
	return
}
func (s *SafeList) InsertBefore(p *Element, v interface{}) (e *Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	e = s.safeList.InsertBefore(v, p)
	return
}
func (s *SafeList) Remove(e *Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		return
	}
	s.safeList.Remove(e)
}
func (s *SafeList) Removes(ele []*Element) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		return
	}
	for _, e := range ele {
		s.safeList.Remove(e)
	}
	return
}
func (s *SafeList) RemoveAll() {
	s.mu.Unlock()
	s.safeList = list.New()
	s.mu.Unlock()
}
func (s *SafeList) Clear() {
	s.RemoveAll()
}
func (s *SafeList) RLockFunc(f func(list *list.List)) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList != nil {
		f(s.safeList)
	}
}
func (s *SafeList) LockFunc(f func(list *list.List)) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	f(s.safeList)
}
func (s *SafeList) Iterator(f func(e *Element) bool) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	if s.safeList == nil {
		return
	}
	length := s.safeList.Len()
	if length > 0 {
		for i, e := 0, s.safeList.Front(); i < length; i, e = i+1, e.Next() {
			if !f(e) {
				break
			}
		}
	}
}
func (s *SafeList) IteratorDesc(f func(e *Element) bool) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	if s.safeList == nil {
		return
	}
	length := s.safeList.Len()
	if length > 0 {
		for i, e := 0, s.safeList.Back(); i < length; i, e = i+1, e.Prev() {
			if !f(e) {
				break
			}
		}
	}
}
func (s *SafeList) Join(glue string) string {
	s.mu.RLock()
	defer s.mu.RUnlock()
	if s.safeList == nil {
		return ""
	}
	buffer := bytes.NewBuffer(nil)
	length := s.safeList.Len()
	if length > 0 {
		for i, e := 0, s.safeList.Front(); i < length; i, e = i+1, e.Next() {
			buffer.WriteString(convert.String(e.Value))
			if i != length-1 {
				buffer.WriteString(glue)
			}
		}
	}
	return buffer.String()
}
func (s *SafeList) String() string {
	return "[" + s.Join(",") + "]"
}
// MarshalJSON implements the interface MarshalJSON for json.Marshal.
func (s *SafeList) MarshalJSON() ([]byte, error) {
	return json.Marshal(s.FrontAll())
}
// UnmarshalJSON implements the interface UnmarshalJSON for json.Unmarshal.
func (s *SafeList) UnmarshalJSON(b []byte) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	var array []interface{}
	if err := json.Unmarshal(b, &array); err != nil {
		return err
	}
	s.PushBacks(array)
	return nil
}
// UnmarshalValue is an interface implement which sets any type of value for list.
func (s *SafeList) UnmarshalValue(value interface{}) (err error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.safeList == nil {
		s.safeList = list.New()
	}
	var array []interface{}
	switch value.(type) {
	case string, []byte:
		err = json.Unmarshal(convert.Bytes(value), &array)
	default:
		 array = convert.SliceAny(value)
	}
	s.PushBacks(array)
	return err
}