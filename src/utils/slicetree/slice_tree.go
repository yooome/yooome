package slicetree

import (
	"fmt"
	"reflect"
	"yooome/utils/convert"
)

// 有层级关系的数组，将子集压入到父级（树形结构）
func PushSonToParent(list []map[string]interface{}, params ...interface{}) []map[string]interface{} {
	args := make([]interface{}, 7)
	for k, v := range params {
		if k == 7 {
			break
		}
		args[k] = v
	}
	var (
		pid         string      // 父级id
		fieldName   string      // 父级 id 键名
		id          string      // id 键名
		key         string      // 子级数组键名
		filter      string      // 过滤键名
		filterVal   interface{} // 过滤的值
		showNoChild bool        // 是否显示不存在的子级键
	)
	pid = convert.String(GetSliceByKey(args, 0, 0))
	fieldName = convert.String(GetSliceByKey(args, 1, "pid"))
	id = convert.String(GetSliceByKey(args, 2, "id"))
	key = convert.String(GetSliceByKey(args, 3, "children"))
	filter = convert.String(GetSliceByKey(args, 4, ""))
	filterVal = GetSliceByKey(args, 5, nil)
	showNoChild = convert.Bool(GetSliceByKey(args, 6, true))

	var returnList []map[string]interface{}
	for _, v := range list {
		if convert.String(v[fieldName]) == pid {
			if filter != "" {
				if reflect.DeepEqual(v[filter], filterVal) {
					args[0] = v[id]
					child := PushSonToParent(list, args...)
					if child != nil || showNoChild {
						v[key] = child
					}
					returnList = append(returnList, v)
				}
			} else {
				args[0] = v[id]
				child := PushSonToParent(list, args...)
				if child != nil || showNoChild {
					v[key] = child
				}
				returnList = append(returnList, v)
			}
		}
	}
	return returnList
}

func GetSliceByKey(args []interface{}, key int, val interface{}) interface{} {
	var value interface{}
	if args[key] != nil {
		value = args[key]
	} else {
		value = val
	}
	return value
}

func ParentSonSort(menuMap []map[string]interface{}, params ...interface{}) []map[string]interface{} {
	args := make([]interface{}, 8)
	for index, value := range params {
		if index == 8 {
			break
		}
		args[index] = value
	}
	var (
		pid       int64  //父级id
		level     int64  // 层级数
		fieldName string // 父级id 键名
		id        string // id键名
		levelName string //层级名称
		title     string // 标题名称
		breaks    int64  // 中断层级
		prefixStr string // 字符串前缀
	)
	pid = convert.Int64(GetSliceByKey(args, 0, 0))
	level = convert.Int64(GetSliceByKey(args, 1, 0))
	fieldName = convert.String(GetSliceByKey(args, 2, "pid"))
	id = convert.String(GetSliceByKey(args, 3, "id"))
	levelName = convert.String(GetSliceByKey(args, 4, "flg"))
	title = convert.String(GetSliceByKey(args, 5, "title"))
	breaks = convert.Int64(GetSliceByKey(args, 6, -1))
	prefixStr = convert.String(GetSliceByKey(args, 7, "─"))
	// 定义一个slice 用于返回
	var menuList []map[string]interface{}
	for _, v := range menuMap {
		if pid == convert.Int64(v[fieldName]) {
			v[levelName] = level
			levelClone := level
			titlePrefix := ""
			for {
				if levelClone < 0 {
					break
				}
				titlePrefix += prefixStr
				levelClone--
			}
			titlePrefix = "├" + titlePrefix
			if level == 0 {
				v["title_prefix"] = ""
			} else {
				v["title_prefix"] = titlePrefix
			}
			v["title_show"] = fmt.Sprintf("%s%s", v["title_prefix"], v[title])
			menuList = append(menuList, v)
			if breaks != -1 && breaks == level {
				continue
			}
			args[0] = v[id]
			args[1] = level + 1
			menuList2 := ParentSonSort(menuMap, args...)
			if len(menuList2) > 0 {
				menuList = append(menuList, menuList2...)
			}
		}
	}
	return menuList
}

// 有层级关系的切片，通过父级 id 查询所有子级 id 数组
func FindSonByParentId(list []map[string]interface{}, parentId int64, parentIndex, idIndex string) []map[string]interface{} {
	newList := make([]map[string]interface{}, 0, len(list))
	for _, v := range list {
		if convert.Int64(v[parentIndex]) == parentId {
			newList = append(newList, v)
			fList := FindSonByParentId(list, convert.Int64(v[idIndex]), parentIndex, idIndex)
			newList = append(newList, fList...)
		}
	}
	return newList
}
