package utilsmap

import (
	"encoding/json"
	"yooome/utils/convert"
	"yooome/utils/rwmutex"
	isempty "yooome/utils/utilsisempty"
)

type AnyAnyMap struct {
	mu   rwmutex.RWMutex
	data map[interface{}]interface{}
}

func NewAnyAnyMap(safe ...bool) *AnyAnyMap {
	return &AnyAnyMap{
		mu:   rwmutex.Create(safe...),
		data: make(map[interface{}]interface{}),
	}
}
func NewAnyAnyMapFrom(data map[interface{}]interface{}, safe ...bool) *AnyAnyMap {
	return &AnyAnyMap{
		mu:   rwmutex.Create(safe...),
		data: data,
	}
}
func (m *AnyAnyMap) Iterator(f func(k interface{}, v interface{}) bool) {
	m.mu.RLock()
	defer m.mu.RUnlock()
	for key, val := range m.data {
		if !f(key, val) {
			break
		}
	}
}
func (m *AnyAnyMap) Clone(safe ...bool) *AnyAnyMap {
	return NewAnyAnyMap(safe...)
}
func (m *AnyAnyMap) Map() map[interface{}]interface{} {
	m.mu.RLock()
	defer m.mu.RUnlock()
	if !m.mu.IsSafe() {
		return m.data
	}
	data := make(map[interface{}]interface{}, len(m.data))
	for k, v := range m.data {
		data[k] = v
	}
	return data
}
func (m *AnyAnyMap) MapCopy() map[interface{}]interface{} {
	m.mu.RLock()
	defer m.mu.RUnlock()
	data := make(map[interface{}]interface{}, len(m.data))
	for k, v := range m.data {
		data[k] = v
	}
	return data
}
func (m *AnyAnyMap) MapStrAny() map[string]interface{} {
	m.mu.Lock()
	defer m.mu.Unlock()
	data := make(map[string]interface{}, len(m.data))
	for k, v := range m.data {
		data[convert.String(k)] = v
	}
	return data
}
func (m *AnyAnyMap) FilterNil() {
	m.mu.Lock()
	defer m.mu.Unlock()
	for k, v := range m.data {
		if isempty.IsNil(v) {
			delete(m.data, k)
		}
	}
}
func (m *AnyAnyMap) Set(key interface{}, value interface{}) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data == nil {
		m.data = make(map[interface{}]interface{})
	}
	m.data[key] = value
}
func (m *AnyAnyMap) Sets(data map[interface{}]interface{}) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data == nil {
		m.data = make(map[interface{}]interface{})
	} else {
		for key, val := range data {
			m.data[key] = val
		}
	}
}
func (m *AnyAnyMap) Search(key interface{}) (val interface{}, find bool) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data != nil {
		val, find = m.data[key]
	}
	return
}
func (m *AnyAnyMap) Get(key interface{}) (val interface{}) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data != nil {
		val = m.data[key]
	}
	return
}
func (m *AnyAnyMap) Pops(size int) map[interface{}]interface{} {
	m.mu.Lock()
	defer m.mu.Unlock()
	if size > len(m.data) || size == -1 {
		size = len(m.data)
	}
	if size == 0 {
		return nil
	}
	var index = 0
	var data = make(map[interface{}]interface{})
	for k, v := range m.data {
		delete(m.data, k)
		data[k] = v
		index++
		if index == size {
			break
		}
	}
	return data
}
func (m *AnyAnyMap) Pop() (key interface{}, value interface{}) {
	m.mu.Lock()
	defer m.mu.Unlock()
	for key, value = range m.data {
		delete(m.data, key)
		return
	}
	return
}
func (m *AnyAnyMap) doSetWithLockCheck(key, value interface{}) interface{} {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data == nil {
		m.data = make(map[interface{}]interface{})
	}
	if v, ok := m.data[key]; ok {
		return v
	}
	if f, ok := value.(func() interface{}); ok {
		value = f()
	}
	if value != nil {
		m.data[key] = value
	}
	return value
}
func (m *AnyAnyMap) GetOrSet(key, value interface{}) interface{} {
	if v, ok := m.Search(key); ok {
		return m.doSetWithLockCheck(key, value)
	} else {
		return v
	}
}
func (m *AnyAnyMap) GetOrSetFunc(key interface{}, f func() interface{}) interface{} {
	if v, ok := m.Search(key); !ok {
		return m.doSetWithLockCheck(key, f())
	} else {
		return v
	}
}
func (m *AnyAnyMap) GetOrSetFuncLock(key interface{}, f func() interface{}) interface{} {
	if v, ok := m.Search(key); ok {
		return m.doSetWithLockCheck(key, f())
	} else {
		return v
	}
}
func (m *AnyAnyMap) SetIfNotExist(key interface{}, value interface{}) bool {
	if !m.Contains(key) {
		m.doSetWithLockCheck(key, value)
		return true
	}
	return false
}
func (m *AnyAnyMap) SetIfNotExistFunc(key interface{}, f func() interface{}) bool {
	if !m.Contains(key) {
		m.doSetWithLockCheck(key, f())
		return true
	}
	return false
}
func (m *AnyAnyMap) SetIfNotExistFuncLock(key interface{}, f func() interface{}) bool {
	if !m.Contains(key) {
		m.doSetWithLockCheck(key, f())
		return true
	}
	return false
}
func (m *AnyAnyMap) Remove(key interface{}) (value interface{}) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data != nil {
		var ok bool
		if value, ok = m.data[key]; ok {
			delete(m.data, key)
		}
	}
	return value
}
func (m *AnyAnyMap) Removes(keys []interface{}) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data != nil {
		for _, key := range keys {
			delete(m.data, key)
		}
	}
}
func (m *AnyAnyMap) Contains(key interface{}) bool {
	var ok bool
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data != nil {
		_, ok = m.data[key]
	}
	return ok
}
func (m *AnyAnyMap) Keys() []interface{} {
	m.mu.Lock()
	defer m.mu.Unlock()
	var (
		keys  = make([]interface{}, len(m.data))
		index = 0
	)
	for key := range m.data {
		keys[index] = key
		index++
	}
	return keys
}

func (m *AnyAnyMap) Values() []interface{} {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data == nil {
		return nil
	}
	values := make([]interface{}, len(m.data))
	index := 0
	for _, val := range m.data {
		values[index] = val
		index++
	}
	return values
}
func (m *AnyAnyMap) Size() int {
	m.mu.Lock()
	defer m.mu.Unlock()
	return len(m.data)
}
func (m *AnyAnyMap) IsEmpty() bool {
	return m.Size() == 0
}
func (m *AnyAnyMap) Clear() {
	m.mu.Lock()
	m.data = make(map[interface{}]interface{})
	m.mu.Unlock()
}
func (m *AnyAnyMap) Replace(data map[interface{}]interface{}) {
	m.mu.Lock()
	m.data = data
	m.mu.Unlock()
}
func (m *AnyAnyMap) LockFunc(f func(m map[interface{}]interface{})) {
	m.mu.Lock()
	defer m.mu.Unlock()
	f(m.data)
}
func (m *AnyAnyMap) RLockFunc(f func(m map[interface{}]interface{})) {
	m.mu.Lock()
	defer m.mu.Unlock()
	f(m.data)
}

// Flip exchanges key-value of the map to value-key.
func (m *AnyAnyMap) Flip() {
	m.mu.Lock()
	defer m.mu.Unlock()
	n := make(map[interface{}]interface{}, len(m.data))
	for k, v := range m.data {
		n[v] = k
	}
	m.data = n
}

// Merge merges two hash maps.
// The <other> map will be merged into the map <m>.
func (m *AnyAnyMap) Merge(other *AnyAnyMap) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data == nil {
		m.data = other.MapCopy()
		return
	}
	if other != m {
		other.mu.RLock()
		defer other.mu.RUnlock()
	}
	for k, v := range other.data {
		m.data[k] = v
	}
}
func (m *AnyAnyMap) String() string {
	b, _ := m.MarshalJSON()
	return convert.String(b)
}

// MarshalJSON implements the interface MarshalJSON for json.Marshal.
func (m *AnyAnyMap) MarshalJSON() ([]byte, error) {
	return json.Marshal(convert.Map(m.Map()))
}
// UnmarshalJSON implements the interface UnmarshalJSON for json.Unmarshal.
func (m *AnyAnyMap) UnmarshalJSON(b []byte) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data == nil {
		m.data = make(map[interface{}]interface{})
	}
	var data map[string]interface{}
	if err := json.Unmarshal(b, &data); err != nil {
		return err
	}
	for k, v := range data {
		m.data[k] = v
	}
	return nil
}
// UnmarshalValue is an interface implement which sets any type of value for map.
func (m *AnyAnyMap) UnmarshalValue(value interface{}) (err error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	if m.data == nil {
		m.data = make(map[interface{}]interface{})
	}
	for k, v := range convert.Map(value) {
		m.data[k] = v
	}
	return
}
