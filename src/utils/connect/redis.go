package connect

import (
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	"reflect"
	"time"
	"yooome/utils/yml_config"
	"yooome/utils/yml_config/ymlconfig_interface"
)

type Redis struct {
	pool *redis.Pool
}
type Conn struct {
	redis.Conn
}

var configYml ymlconfig_interface.YmlConfigInterface

func init() {
	configYml = yml_config.NewYamlFileFactory()
}
func InitRedis() *Redis {
	initRedis := &Redis{
		pool: &redis.Pool{
			MaxIdle:     configYml.GetInt("Redis.MaxIdle"),
			MaxActive:   configYml.GetInt("Redis.MaxActive"),
			IdleTimeout: configYml.GetDuration("Redis.IdleTimeout") * time.Second,
			Dial: func() (redis.Conn, error) {
				conn, err := redis.Dial("tcp", configYml.GetString("Redis.Host")+":"+configYml.GetString("Redis.Port"))
				if err != nil {
					logrus.Info("redis dial conn error ", err)
					return nil, err
				}
				// 通过配置设置密码
				auth := configYml.GetString("Redis.Auth") //通过配置项设置redis密码
				if len(auth) >= 1 {
					if _, err := conn.Do("AUTH", auth); err != nil {
						_ = conn.Close()
						logrus.Info("redis commandName err ", err)
					}
				}
				_, _ = conn.Do("SELECT", configYml.GetString("Redis.IndexDb"))
				return conn, err
			},
		},
	}
	return initRedis
}

func (r *Redis) Do(commandName string, args ...interface{}) (reply interface{}, err error) {
	return r.DoConn(0, commandName, args...)
}

func (r *Redis) DoConn(timeOut time.Duration, commandName string, args ...interface{}) (reply interface{}, err error) {
	var (
		reflectValue reflect.Value
		reflectKind  reflect.Kind
	)
	for k, v := range args {
		reflectValue = reflect.ValueOf(v)
		reflectKind = reflectValue.Kind()
		if reflectKind == reflect.Ptr {
			reflectValue = reflectValue.Elem()
			reflectKind = reflectValue.Kind()
		}
		switch reflectKind {
		case
			reflect.Struct,
			reflect.Map,
			reflect.Slice,
			reflect.Array:
			if _, ok := v.([]byte); !ok {
				if args[k], err = json.Marshal(v); err != nil {
					return nil, err
				}
			}

		}
	}
	if timeOut > 0 {
		conn := r.pool.Get().(redis.ConnWithTimeout)
		return conn.DoWithTimeout(timeOut, commandName, args...)
	}
	conn := r.pool.Get()
	// r.pool.Get().Do()
	// redis.Conn().
	defer conn.Close()
	// return conn.Do("HMSet", "user1", "name", "beijing", "address", "beijing")
	return conn.Do(commandName, args...)
}
