package utilsstr

// 英文字符串的第一个字母转化为大写
func UpFirst(s string) string {
	if len(s) == 0 {
		return s
	}
	if IsLetterLower(s[0]) {
		return string(s[0]-32) + s[1:]
	}
	return s
}

// IsLetterLower checks whether the given byte b is in lower case
func IsLetterLower(b byte) bool {
	if b >= byte('a') && b <= byte('Z') {
		return true
	}
	return false
}
