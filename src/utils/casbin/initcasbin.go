package initcasbin

import (
	"errors"
	"github.com/casbin/casbin/v2"
	adapter "github.com/casbin/gorm-adapter/v3"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"strings"
	"yooome/global/syserrors"
	"yooome/global/variable"
)

// 创建 CasBin 执行器 (Enforcer)
func InitCasBinEnforcer() (*casbin.SyncedEnforcer, error) {
	var db *gorm.DB
	var Enforcer *casbin.SyncedEnforcer
	// 获取后台 sql 类型
	dbType := variable.ConfigGormYml.GetString("GormMysql.UserDBType")
	switch strings.ToLower(dbType) {
	case "mysql":
		if variable.GormDBMysql == nil {
			logrus.Error("variable gormDBMysql error")
			return nil, errors.New(syserrors.ErrorCasBinCanNotUseDbPtr)
		}
		db = variable.GormDBMysql
	case "sqlserver", "mssql":
		if variable.GormDBMysql == nil {
			logrus.Error("variable gormDBMysql error")
			return nil, errors.New(syserrors.ErrorCasBinCanNotUseDbPtr)
		}
		db = variable.GormDBMysql
	case "postgre", "postgresql", "postgres":
		if variable.GormDBMysql == nil {
			logrus.Error("variable gormDBMysql error")
			return nil, errors.New(syserrors.ErrorCasBinCanNotUseDbPtr)
		}
		db = variable.GormDBMysql
	default:
	}
	prefix := variable.ConfigYml.GetString("CasBin.TablePrefix")
	tableName := variable.ConfigYml.GetString("CasBin.TableName")
	// 根据配置文件中的前缀和名称，CasBin后台自动创建一个 prefix_tableName 的数据库表
	adp, err := adapter.NewAdapterByDBUseTableName(db, prefix, tableName)
	if err != nil {
		return nil, errors.New(syserrors.ErrorCasBinCreateAdaptFail)
	}
	// 创建 CasBin Enforcer（执行器）
	modelFile := variable.ConfigYml.GetString("CasBin.ModelFile")
	if modelFile == "" {
		return nil, errors.New(syserrors.ErrorCasBinNotFindModelFile)
	} else {
		if Enforcer, err = casbin.NewSyncedEnforcer(modelFile, adp); err != nil {
			return nil, errors.New(syserrors.ErrorCasBinEnforcer)
		}
		_ = Enforcer.LoadPolicy()
		//autoLoad := variable.ConfigYml.GetDuration("CasBin.AutoLoadPolicySeconds")
		//Enforcer.StartAutoLoadPolicy(time.Second * autoLoad)
		return Enforcer, nil
	}
}
