package tools

import (
	"encoding/base64"
	"fmt"
	"github.com/sirupsen/logrus"
	"net"
	"time"
	"yooome/utils/convert"
	"yooome/utils/crypto/aes"
)

// 字符串加密
func EncryptCBC(plainText, publicKey string) string {
	key := []byte(publicKey)
	bts, err := aes.EncryptCBC([]byte(plainText), key, key)
	if err != nil {
		logrus.Error("aes encryptCBC err ", err)
		return ""
	}
	dst := make([]byte, base64.StdEncoding.EncodedLen(len(bts)))
	base64.StdEncoding.Encode(dst, bts)
	return convert.String(dst)
}

// 字符串解密
func DecryptCBC(plainText, publicKey string) string {
	key := []byte(publicKey)
	data := []byte(plainText)
	plainTextByte := make([]byte, base64.StdEncoding.DecodedLen(len(data)))
	n, err := base64.StdEncoding.Decode(plainTextByte, data)
	if err != nil {
		logrus.Error("base64 stdEncoding decode err ", err)
		return ""
	}
	b, e := aes.DecryptCBC(plainTextByte[:n], key, key)
	if e != nil {
		logrus.Error("aes decryptCBC err ", e)
		return ""
	}
	fmt.Println("plainText = ", convert.String(b))
	dst := make([]byte, base64.StdEncoding.EncodedLen(len(b)))
	base64.StdEncoding.Encode(dst, b)
	return convert.String(dst)
}
func GetLocalIP() (ip string, err error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return
	}
	for _, addr := range addrs {
		ipAddr, ok := addr.(*net.IPNet)
		if !ok {
			continue
		}
		if !ipAddr.IP.IsLoopback() {
			continue
		}
		if !ipAddr.IP.IsGlobalUnicast() {
			continue
		}
		return ipAddr.IP.String(), nil
	}
	return
}

func GetHourDiffer(startTime, endTime string) int64 {
	var hour int64
	t1, err := time.ParseInLocation("2006-01-02 15:04:05", startTime, time.Local)
	t2, err := time.ParseInLocation("2006-01-02 15:04:05", endTime, time.Local)
	if err == nil && t1.Before(t2) {
		diff := t2.Unix() - t1.Unix()
		hour = diff / 3600
		return hour
	} else {
		return hour
	}
}
