package cmsmenu_service

import (
	"yooome/model/cmsmenu_model"
	"yooome/model/cmsmenu_params"
	"yooome/model/cmsnews_model"
	"yooome/model/modelinfo_model"
)

func GetCmsMenuList(params *cmsmenu_params.CmsMenuParams) (list []*cmsmenu_model.CmsMenuModel, err error) {
	return cmsmenu_model.NewCmsMenuModelFactory("").GetCmsMenuList(params)
}
func GetTreeSelect() (list []*cmsmenu_model.CmsMenuModel, err error) {
	// 获取频道列表
	listAll, err := cmsmenu_model.NewCmsMenuModelFactory("").GetTreeSelect()
	list = make([]*cmsmenu_model.CmsMenuModel, 0, len(listAll))
	for _, v := range listAll {
		if v.Status == 1 && v.CateType == 1 {
			list = append(list, v)
		}
	}
	return list, nil
}
func GetCmsTemplate() ([]string, []string) {
	var listTemplates []string
	var contentTemplates []string
	listTemplates = append(listTemplates, "list/list.html")
	contentTemplates = append(contentTemplates, "content/show.html")
	return listTemplates, contentTemplates
}
func GetModelByCateIds(cateIds []int64) (mode []*modelinfo_model.ModelInfo, err error) {
	return modelinfo_model.NewModelInfoFactory("").GetModelByCateIds(cateIds)
}
func AddModelOptions(params *cmsmenu_params.AddMenuParams) bool {
	return modelinfo_model.NewModelInfoFactory("").AddModelOptions(params)
}
func GetMenuById(id string) (mode *cmsmenu_model.CmsMenuModel, err error) {
	return modelinfo_model.NewModelInfoFactory("").GetMenuById(id)
}
func GetMenuListChannel() (list []*cmsmenu_model.CmsMenuModel, err error) {
	return modelinfo_model.NewModelInfoFactory("").GetMenuListChannel()
}
func UpdateCmsCategory(params *cmsmenu_params.UpdateMenuParams) bool {
	return modelinfo_model.NewModelInfoFactory("").UpdateCmsCategory(params)
}
func DeleteCategory(ids []int64) bool {
	return modelinfo_model.NewModelInfoFactory("").DeleteCategory(ids)
}
func GetListNews(param *cmsmenu_params.QueryNewsParams, m map[string]interface{}) (total, page int, list []*cmsnews_model.NewModel, err error) {
	return cmsnews_model.NewNewsModelFactory("").GetListNews(param, m)
}
