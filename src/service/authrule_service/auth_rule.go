package authrule_service

import (
	"yooome/model/auth_model"
	"yooome/model/auth_rule"
)

// 获取所有的按钮 isMenu=2，且status=1的菜单列表
func GetIsButtonStatusList() ([]*auth_rule.AuthRule, error) {
	list, err := GetMenuList()
	if err != nil {
		return nil, err
	}
	var authList = make([]*auth_rule.AuthRule, len(list))
	for _, v := range authList {
		if v.MenuType == 2 && v.Status == 1 {
			// 获取 button 的状态
			authList = append(authList, v)
		}
	}
	return list, err
}

func GetMenuList() (list []*auth_rule.AuthRule, err error) {
	return auth_rule.NewAuthRuleFactory("").GetMenuList()
}

// 根据查询查询参数获取 菜单列表
func GetMenuListByParams(queryParams *auth_model.GetMenuListParams) (list []*auth_rule.AuthRule, err error) {
	return auth_rule.NewAuthRuleFactory("").GetMenuListByParams(queryParams)
}

// 获取isMenu=0|1 且 status = 1 的菜单列表
func GetIsMenuStatusList() ([]*auth_rule.AuthRule, error) {
	list, err := GetMenuList()
	if err != nil {
		return nil, err
	}
	var ruleList = make([]*auth_rule.AuthRule, 0, len(list))
	for _, v := range list {
		if (v.MenuType == 0 || v.MenuType == 1) && v.Status == 1 {
			ruleList = append(ruleList, v)
		}
	}
	return ruleList, err
}

// 根据id获取 菜单detail
func GetMenuDetailById(menuId string) (menuDetail *auth_rule.AuthRule, err error) {
	return auth_rule.NewAuthRuleFactory("").GetMenuDetailById(menuId)
}

func CheckMenuNameUnique(name string, id int) bool {
	return auth_rule.NewAuthRuleFactory("").CheckMenuNameUnique(name, id)
}
func CheckMenuPathUnique(path string, id int) bool {
	return auth_rule.NewAuthRuleFactory("").CheckMenuPathUnique(path, id)
}

// 更新数据库中的数据
func UpdateMenuById(menus *auth_model.UpdateMenuParams) bool {
	return auth_rule.NewAuthRuleFactory("").UpdateMenuById(menus)
}

// 添加数据到数据库
func AddMenu(addMenu *auth_model.AddMenuParams) bool {
	return auth_rule.NewAuthRuleFactory("").AddMenu(addMenu)
}
func DeleteMenu(id int) bool {
	return auth_rule.NewAuthRuleFactory("").DeleteMenu(id)
}
