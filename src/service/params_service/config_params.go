package params_service

import (
	"yooome/model/params_model"
	"yooome/model/sys_config_params"
)

// 获取系统参数
func GetParamsList(params *sys_config_params.ParamsPageReq) (total int, page int, list []*params_model.ConfigParams, err error) {
	return params_model.NewConfigParamsFactory("").GetParamsList(params)
}

// 检查 configKey 是否唯一
func CheckConfigKeyUnique(configKey string) bool {
	return params_model.NewConfigParamsFactory("").CheckConfigKeyUnique(configKey)
}

// 添加 参数数据
func AddConfigParams(params *sys_config_params.AddParamsReq, userId int64) int64 {
	return params_model.NewConfigParamsFactory("").AddConfigParams(params, userId)
}
func UpdateConfig(params *sys_config_params.UpdParamsReq, userId int64) int64 {
	return params_model.NewConfigParamsFactory("").UpdateConfig(params, userId)
}

// 根据config_id 获取参数数据
func GetConfigDetailById(id uint64) *params_model.ConfigParams {
	return params_model.NewConfigParamsFactory("").GetConfigDetailById(id)
}

// 根据 config_id 删除参数数据
func DelConfig(configIds []int) bool {
	return params_model.NewConfigParamsFactory("").DelConfig(configIds)
}
