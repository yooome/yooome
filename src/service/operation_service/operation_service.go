package operation_service

import (
	"yooome/model/operation_model"
	"yooome/model/operation_params"
)

func GetOperationLogList(params *operation_params.OperationParams) (total, page int, list []*operation_model.OperationModel, err error) {
	return operation_model.NewOperationModelFactory("").GetOperationLogList(params)
}
func DelOperationLog(ids []int) bool {
	return operation_model.NewOperationModelFactory("").DelOperationLog(ids)
}
