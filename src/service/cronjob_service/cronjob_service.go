package cronjob_service

import (
	"yooome/model/job_model"
	"yooome/model/job_params"
)

func GetJobList(params *job_params.QueryJobParams) (total, page int, list []*job_model.Job, err error) {
	return job_model.NewJobFactory("").GetJobList(params)
}
func AddJob(params *job_params.AddJobParams, userId int64) bool {
	return job_model.NewJobFactory("").AddJob(params, userId)
}
func GetJobById(id string) (job *job_model.Job, err error) {
	return job_model.NewJobFactory("").GetJobById(id)
}
func UpdateJob(params *job_params.UpdateJobParams, userId int) bool {
	return job_model.NewJobFactory("").UpdateJob(params, userId)
}
func DelJobById(id string) bool {
	return job_model.NewJobFactory("").DelJobById(id)
}
