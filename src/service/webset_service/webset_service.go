package webset_service

import "yooome/model/webset_model"

// 获取 webset 数据
func GteInfo() *webset_model.WebSet {
	return webset_model.NewWebSetFactory().GetInfo()
}
func UpdateInfo(content string, id int) bool {
	return webset_model.NewWebSetFactory().UpdateInfo(content, id)
}
