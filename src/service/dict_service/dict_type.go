package dict_service

import (
	"yooome/model/dict_model"
	"yooome/model/sys_dict_type"
)

// 字典列表查询匪类
func GetDictListByPage(pageReq *sys_dict_type.PageReqParams) (total, page int, list []*dict_model.Dict, err error) {
	return dict_model.NewDicModelFactory("").GetDictListByPage(pageReq)
}

// 通过字典健类型获取选项
func GetStatusByDictType(dictType string, defaultValue string, emptyLabel string) (map[string]interface{}, error) {
	return dict_model.NewDicModelFactory("").GetStatusByDictType(dictType, defaultValue, emptyLabel)
}

func AddDict(addDictType *sys_dict_type.AddDictType, userId int64) (result int64, err error) {
	return dict_model.NewDicModelFactory("").AddDict(addDictType, userId)
}

func GetDetail(dictId string) (*dict_model.Dict, error) {
	return dict_model.NewDicModelFactory("").GetDetail(dictId)
}

func UpdateDict(updateDict *sys_dict_type.UpdateDictType, userId int64) (result int64, err error) {
	return dict_model.NewDicModelFactory("").UpdateDict(updateDict, userId)
}

func DeleteByDictId(dictIds []int) bool {
	return dict_model.NewDicModelFactory("").DeleteByDictIds(dictIds)
}
