package variable

import (
	"github.com/casbin/casbin/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"log"
	"os"
	"strings"
	"yooome/global/syserrors"
	"yooome/utils/snow_flake/snowflake_interf"
	"yooome/utils/yml_config/ymlconfig_interface"
)

// 封装的全局变量全部支持并发安全，请放心使用
// 开发者自己封装的全局变量，请做好并发安全检查与确认
var (
	BasePath string // 定义项目的根目录
	// EventDestoryPrefix = "Destory_" // 程序退出时需要销毁的事件前缀
	ConfigKeyPrefix = "Config_" //  配置文件键值缓存时，键的前缀

	ConfigYml     ymlconfig_interface.YmlConfigInterface // 全局配置文件指针
	ConfigGormYml ymlconfig_interface.YmlConfigInterface // 全局配置文件指针
	Logrus        *logrus.Logger
	//gorm 数据库客户端，如果您操作数据库使用的是gorm，请取消以下注释，在 bootstrap>init 文件，进行初始化即可使用
	GormDBMysql *gorm.DB // 全局gorm的客户端连接
	//雪花算法全局变量
	SnowFlake snowflake_interf.SnowFlakeInterface

	//CasBin 全局操作指针
	Enforcer *casbin.SyncedEnforcer
	// 无需验证权限的用户 id
	NotCheckAuthAdminIds []int
)

func init() {
	// 1.初始化程序根目录
	path, err := os.Getwd()
	if err != nil {
		log.Fatal(syserrors.ErrorsBasePath)
		return
	}
	// 路径进行处理，兼容单元测试程序程序启动的时候
	if len(os.Args) > 1 && strings.HasPrefix(os.Args[1], "-test") {
		BasePath = strings.Replace(strings.Replace(path, `\test`, "", 1), `/test`, "", 1)
	} else {
		BasePath = path
	}

}
