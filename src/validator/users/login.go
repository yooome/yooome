package users

import (
	"github.com/gin-gonic/gin"
	"net/http"
	userscontroller "yooome/controller/users"
	"yooome/global/contsts"
	"yooome/validator/captcha"
	"yooome/validator/transfers"
)

type Login struct {
	UserName string `form:"username" json:"username"` // 必填、对于文本,表示它的长度>=1
	PassWord string `form:"password" json:"password"` //  密码为 必填，长度>=6
	Bas64    string `form:"bas64" json:"bas64"`       //  密码为 必填，长度>=6
	Id       string `form:"id" json:"id"`             //  密码为 必填，长度>=6
}

func (l *Login) ValidatorParams(context *gin.Context) {
	if err := context.ShouldBind(&l); err != nil {
		errs := gin.H{"message": err}
		context.JSON(http.StatusNotFound, errs)
		return
	}
	// 校验验证码是否正确
	isCaptcha := captcha.ValidCaptcha(l.Bas64, l.Id)
	if !isCaptcha {
		errs := gin.H{"message": "验证码校验失败"}
		context.JSON(http.StatusNotFound, errs)
		return
	}
	contextData := transfers.ContextAddData(l, contsts.ValidatorPrefix, context)
	if contextData == nil {
		errs := gin.H{"message": "err"}
		context.JSON(http.StatusNotFound, errs)
		return
	} else {
		// 验证完成，调用控制器，并将验证器字段完成传递给控制器，保证上下文数据一致
		(&userscontroller.Users{}).Login(contextData)
	}
}
