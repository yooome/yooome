package transfers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"time"
	validInterface "yooome/core/valid_interface"
)
// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
/**
validInterface:实现了验证器接口的结构体
Prefix:验证器绑定参数传递给控制器的数据前缀
context:gin上下文
*/
func ContextAddData(validInterface validInterface.ValidatorInterface, Prefix string, context *gin.Context) *gin.Context {
	var tempJson interface{}
	if tempBytes, err := json.Marshal(validInterface); err == nil {
		if err2 := json.Unmarshal(tempBytes, &tempJson); err2 == nil {
			if value, ok := tempJson.(map[string]interface{}); ok {
				for k, v := range value {
					context.Set(Prefix+k, v)
				}
				// 此外给上下文追加三个键：created_at  、 updated_at  、 deleted_at ，实际根据需要自己选择获取相关键值
				curDateTime := time.Now().Format("2006-01-02 15:04:05")
				context.Set(Prefix+"created_at", curDateTime)
				context.Set(Prefix+"updated_at", curDateTime)
				context.Set(Prefix+"deleted_at", curDateTime)
				return context
			}
		}

	}
	return nil
}
