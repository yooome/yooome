package captcha

import "github.com/mojocn/base64Captcha"

func ValidCaptcha(val string, id string) bool {
	var store = base64Captcha.DefaultMemStore
	if val == "" || id == "" {
		return false
	}
	return store.Verify(id, val, true)
}
