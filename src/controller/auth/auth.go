package authmenu

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"yooome/model/auth_model"
	"yooome/model/auth_rule"
	"yooome/service/authrule_service"
	"yooome/service/dict_service"
	"yooome/utils/convert"
	"yooome/utils/slicetree"
)

// 菜单用户组用户管理
type Auths struct{}

func (a *Auths) GetMenuListByParams(context *gin.Context) {
	// 获取前端的 传递过来的参数
	var params auth_model.GetMenuListParams
	err := context.ShouldBind(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	fmt.Println("isSuccess:", isSuccess)
	authMenu, err := authrule_service.GetMenuListByParams(&params)
	menus := make([]map[string]interface{}, len(authMenu))
	for index, value := range authMenu {
		data := convert.Map(value)
		menus[index] = data
	}
	if &params != nil {
		for k := range menus {
			menus[k]["children"] = nil
		}
	} else {
		menus = slicetree.PushSonToParent(menus, 0, "pid", "id", "children", "", nil, true)
	}
	// 菜单显示状态
	options, err := dict_service.GetStatusByDictType("sys_show_hide", "", "")
	if err != nil {
		isSuccess = false
		fmt.Println("get status by dict type error", err)
	}
	// 菜单正常 or 停用状态
	statusOptions, err := dict_service.GetStatusByDictType("sys_normal_disable", "", "")
	if err != nil {
		isSuccess = false
	}
	data := map[string]interface{}{
		"isSuccess":      isSuccess,
		"list":           menus,
		"visibleOptions": options,
		"statusOptions":  statusOptions,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 根据 id 获取菜单详情
func (a *Auths) GetMenuDetailById(context *gin.Context) {
	// 获取菜单的  id
	menuId := context.Query("menuId")
	isSuccess := true
	if menuId == "" {
		isSuccess = false
	}
	menuDetail, err := authrule_service.GetMenuDetailById(menuId)
	if err != nil {
		isSuccess = false
	}
	// 获取父级菜单信息
	menuList, err := authrule_service.GetMenuList()
	if err != nil {
		isSuccess = false
	}
	var menus = make([]*auth_rule.AuthRule, 0, len(menuList))
	for _, v := range menuList {
		if v.MenuType == 0 || v.MenuType == 1 {
			menus = append(menus, v)
		}
	}
	var menusMap = make([]map[string]interface{}, len(menus))
	for k, v := range menus {
		menusMap[k] = convert.Map(v)
	}
	menuSort := slicetree.ParentSonSort(menusMap)
	data := map[string]interface{}{
		"menuDetail": menuDetail,
		"isSuccess":  isSuccess,
		"menu":       menuDetail,
		"parentList": menuSort,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

func (a *Auths) UpdateMenu(context *gin.Context) {
	var updateMenu auth_model.UpdateMenuParams
	err := context.ShouldBind(&updateMenu)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	if &updateMenu == nil {
		isSuccess = false
	}
	fmt.Println("issuccess = ", isSuccess)
	// 判断菜单规则是否存在
	if !authrule_service.CheckMenuNameUnique(updateMenu.Name, updateMenu.MenuId) {
		isSuccess = false
	}
	// 判断路由是否已经存在
	if !authrule_service.CheckMenuPathUnique(updateMenu.Path, updateMenu.MenuId) {
		isSuccess = false
	}
	// 更新数据库中的信息
	isSuccess = authrule_service.UpdateMenuById(&updateMenu)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 添加菜单
func (a *Auths) AddMenu(context *gin.Context) {
	var addMenu auth_model.AddMenuParams
	err := context.ShouldBind(&addMenu)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	fmt.Println(isSuccess)
	if !authrule_service.CheckMenuNameUnique(addMenu.Name, addMenu.MenuId) {
		isSuccess = false
	}
	// 判断路由是否已经存在
	if !authrule_service.CheckMenuPathUnique(addMenu.Path, addMenu.MenuId) {
		isSuccess = false
	}
	// 保存数据到数据库
	isSuccess = authrule_service.AddMenu(&addMenu)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 下拉树选择
func (a *Auths) TreeSelect(context *gin.Context) {
	// 获取父级菜单信息
	menuList, err := authrule_service.GetMenuList()
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	var menus = make([]*auth_rule.AuthRule, 0, len(menuList))
	for _, v := range menuList {
		if v.MenuType == 0 || v.MenuType == 1 {
			menus = append(menus, v)
		}
	}
	var menusMap = make([]map[string]interface{}, len(menus))
	for k, v := range menus {
		menusMap[k] = convert.Map(v)
	}
	menuSort := slicetree.ParentSonSort(menusMap)
	data := map[string]interface{}{
		"isSuccess":  isSuccess,
		"parentList": menuSort,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 菜单管理 删除
func (a *Auths) DeleteMenu(context *gin.Context) {
	var menuIds int
	err := context.ShouldBindJSON(&menuIds)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	if !(menuIds > 0) {
		isSuccess = false
	}
	isSuccess = authrule_service.DeleteMenu(menuIds)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
