package usermanagement

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"yooome/global/variable"
	"yooome/model/sys_dept_model"
	"yooome/model/sys_dept_params"
	"yooome/model/user_params"
	"yooome/service/auth_service"
	"yooome/service/dept_service"
	"yooome/service/dict_service"
	"yooome/service/post_service"
	"yooome/service/userinfo_service"
	"yooome/service/usermanagement_service"
	"yooome/utils/convert"
	"yooome/utils/slicetree"
)

type UserManagement struct {
}

// 获取用户信息
func (u *UserManagement) GetUserList(context *gin.Context) {
	var userParams user_params.GetUserParams
	err := context.ShouldBind(&userParams)
	isSuccess := true
	if userParams.DeptId != "" {
		// 获取部门信息
		deptList, err := dept_service.GetList()
		if err != nil {
			isSuccess = false
		}
		deptMap := convert.SliceMaps(deptList)
		deptId := convert.Int64(userParams.DeptId)
		userParams.DeptIds = append(userParams.DeptIds, deptId)
		childrenIds := slicetree.FindSonByParentId(deptMap, deptId, "parentId", "deptId")
		for _, d := range childrenIds {
			userParams.DeptIds = append(userParams.DeptIds, convert.Int64(d["deptId"]))
		}
	}

	if err != nil {
		isSuccess = false
	}
	if err != nil {
		isSuccess = false
	}
	total, page, list, err := usermanagement_service.GetUserList(&userParams)
	if err != nil {
		logrus.Error("获取用户角色数据失败")
		isSuccess = false
	}
	// 获取所有角色信息
	allRoles, err := auth_service.GetRoleList()
	// 获取所有部门信息
	depts, err := dept_service.GetDeptList(&sys_dept_params.DeptParams{})
	if err != nil {
		isSuccess = false
	}
	users := make([]map[string]interface{}, len(list))
	for index, value := range list {
		var dept *sys_dept_model.Dept
		users[index] = convert.Map(value)
		for _, d := range depts {
			if convert.Uint(value.DeptId) == d.DeptID {
				dept = d
			}
		}
		users[index]["dept"] = dept
		roles, err := userinfo_service.GetAdminRole(value.Id, allRoles)
		if err != nil {
			isSuccess = false
		}
		roleInfo := make([]map[string]interface{}, 0, len(roles))
		for _, r := range roles {
			data := map[string]interface{}{
				"roleId": r.Id,
				"name":   r.Name,
			}
			roleInfo = append(roleInfo, data)
		}
		users[index]["userStatus"] = convert.String(value.UserStatus)
		users[index]["roleInfo"] = roleInfo
	}
	// 用户状态
	statusOptions, err := dict_service.GetStatusByDictType("sys_normal_disable", "", "")
	if err != nil {
		isSuccess = false
	}
	userGender, err := dict_service.GetStatusByDictType("sys_user_sex", "", "")
	if err != nil {
		isSuccess = false
	}
	data := map[string]interface{}{
		"total":         total,
		"currentPage":   page,
		"userList":      users,
		"statusOptions": statusOptions,
		"userGender":    userGender,
		"isSuccess":     isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}
func (u *UserManagement) GetUserInfo(context *gin.Context) {
	// 获取角色信息
	roleList, err := auth_service.GetRoleList()
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 获取岗位信息
	postList, err := post_service.GetListPost()
	if err != nil {
		isSuccess = false
	}
	res := map[string]interface{}{
		"roleList":  roleList,
		"postList":  postList,
		"isSuccess": isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": res,
	})
}
func (u *UserManagement) AddUser(context *gin.Context) {
	var addUserParams user_params.AddUserParams
	err := context.ShouldBindJSON(&addUserParams)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	id := userinfo_service.AddUser(&addUserParams)

	// 设置用户所属角色信息
	isSuccess = auth_service.AddUserRole(addUserParams.RoleIds, id)
	// 设置用户岗位
	isSuccess = userinfo_service.AddUserPost(addUserParams.RoleIds, id)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 查询用户信息
func (u *UserManagement) GetUserInfoById(context *gin.Context) {
	id := context.Query("id")
	// 获取用户信息
	userInfo := userinfo_service.GetUserInfoById(id)
	isSuccess := true
	if userInfo == nil {
		isSuccess = false
	}
	// 获取角色信息
	roleList, err := auth_service.GetRoleList()
	if err != nil {
		isSuccess = false
	}
	// 获取已选择的角色信息
	checkedRoleIds, err := auth_service.GetAdminRoleIds(id)
	if err != nil {
		isSuccess = false
	}
	if checkedRoleIds == nil {
		checkedRoleIds = []int64{}
	}
	// 获取正常状态的信息
	post, err := post_service.GetStatusPost()
	if err != nil {
		isSuccess = false
	}
	// 获取根据id岗位信息
	checkPosts, err := post_service.GetUserPostById(id)
	if err != nil {
		isSuccess = false
	}

	data := map[string]interface{}{
		"roleList":       roleList,
		"userInfo":       userInfo,
		"checkedRoleIds": checkedRoleIds,
		"posts":          post,
		"checkPosts":     checkPosts,
		"isSuccess":      isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 修改信息
func (u *UserManagement) UpdateUser(context *gin.Context) {
	var updateUserParams user_params.UpdateUserParams
	err := context.ShouldBindJSON(&updateUserParams)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 更新用户信息
	isSuccess = userinfo_service.UpdateUserInfo(&updateUserParams)

	// 设置用户所属角色信息
	isSuccess = userinfo_service.UpdateUserRole(updateUserParams.RoleIds, updateUserParams.UserId)
	// 设置用户岗位数据
	isSuccess = userinfo_service.AddUserPost(updateUserParams.PostIds, convert.Int64(updateUserParams.UserId))
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (u *UserManagement) DelUser(context *gin.Context) {
	var userIds []int64
	err := context.ShouldBindJSON(&userIds)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	// 删除tb_user表中的信息
	isSuccess = userinfo_service.DelUser(userIds)
	// 删除对应的权限
	enforcer := variable.Enforcer
	for _, v := range userIds {
		enforcer.RemoveFilteredGroupingPolicy(0, fmt.Sprintf("u_%d", v))
	}

	// 删除用户对应的岗位
	isSuccess = userinfo_service.DelUserPost(userIds)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
func (u *UserManagement) ResetUserPwd(context *gin.Context) {
	var params user_params.ResetPwdRew
	err := context.ShouldBindJSON(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	isSuccess = userinfo_service.ResetUserPwd(&params)
	context.JSON(http.StatusOK, gin.H{
		"data": isSuccess,
	})
}
