package systemInfo

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"yooome/global/variable"
	"yooome/service/auth_service"
	"yooome/service/userinfo_service"
	"yooome/utils/convert"
)

type SystemInfo struct {
}

// @Summary 获取登录用户信息
// @Description 获取登录用户信息
// @Tag 公共
// @Success 0 {object} response.Response "{"code":200,"data":[...]}"
// @Router /system/index/getInfo [get]
// @Security
func (s *SystemInfo) GetInfo(context *gin.Context) {
	// 获取用户信息
	userModel, err := userinfo_service.GetCurrentUser(context)
	if err != nil {
		// 获取用户信息 err
		logrus.Error("get userinfo err", err)
		return
	}
	// 将 userModel 转换为 map 结构
	userInfo := convert.Map(userModel)
	rolesList := make([]string, 10)
	var permission []string
	if userInfo != nil {
		userId := userModel.Id
		userInfo["roles"] = make([]string, 0)

		// 获取用户的角色信息
		allRoles, err := auth_service.GetRoleList()
		if err == nil {
			roles, err := userinfo_service.GetAdminRole(userId, allRoles)
			if err == nil {
				name := make([]string, len(roles))
				roleIds := make([]int64, len(roles))
				for k, v := range roles {
					if v != nil {
						name[k] = v.Name
						roleIds[k] = v.Id
					}
				}
				userInfo["roles"] = roles
				isSuperAdmin := false
				// 获取无需验证权限的用户id
				for _, v := range variable.NotCheckAuthAdminIds {
					if convert.Int64(v) == userId {
						isSuperAdmin = true
						break
					}
				}
				if isSuperAdmin {
					permission = []string{"*/*/*/"}
				} else {
					permission, err = userinfo_service.GetPermissions(roleIds)
				}
				rolesList = name
			} else {
				logrus.Error(err)
			}
		} else {
			logrus.Error(err)
		}
	}
	result := map[string]interface{}{
		"user":        userInfo,
		"roles":       rolesList,
		"permissions": permission,
	}
	// 前端返回 getInfo
	context.JSON(http.StatusOK, gin.H{
		"data": result,
	})
}

// @Summery 获取后台菜单
// @Description 获取后台菜单
// @Tags 公共
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/index/getRouters [get]
// @Security
func (s *SystemInfo) GetRouters(context *gin.Context) {
	// 获取用户信息
	userEntity := userinfo_service.GetLoginAdminInfo(context)
	var menuList []map[string]interface{}
	isSuperAdmin := false
	if userEntity != nil {
		userId := userEntity.Id
		// 获取无需验证权限的用户id
		for _, v := range variable.NotCheckAuthAdminIds {
			if convert.Int64(v) == userId {
				isSuperAdmin = true
				break
			}
		}
		// 获取用户角色信息
		allRoles, err := auth_service.GetRoleList()
		if err == nil {
			roles, err := userinfo_service.GetAdminRole(userId, allRoles)
			if err == nil {
				name := make([]string, len(roles))
				roleIds := make([]int64, len(roles))
				for index, value := range roles {
					name[index] = value.Name
					roleIds[index] = value.Id
				}
				// 获取菜单信息
				if isSuperAdmin {
					// 超级管理员获取所有的菜单
					menuList, err = userinfo_service.GetAllMenus()
				} else {
					menuList, err = userinfo_service.GetAdminMenusByRoleIds(roleIds)
				}
				if err != nil {
					logrus.Error("super admin get menu error", err)
				}
			} else {
				logrus.Error("get admin role error ", err)
			}

		} else {
			logrus.Error("get role list error ", err)
		}
	}
	if menuList == nil {
		menuList = []map[string]interface{}{}
	}

	// 给前端返回 menuList 信息
	context.JSON(http.StatusOK, gin.H{
		"data": map[string]interface{}{
			"menuList": menuList,
		},
	})
}
