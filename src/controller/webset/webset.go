package configwebset

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"yooome/model/webset_model"
	"yooome/service/webset_service"
)

// 创建结构体
type ConfigWebSet struct{}

func (c *ConfigWebSet) GetInfo(context *gin.Context) {
	// 获取 webset 数据
	webSet := webset_service.GteInfo()
	isSuccess := true
	if webSet == nil {
		isSuccess = false
	}
	context.JSON(http.StatusOK, gin.H{
		"data":      webSet,
		"isSuccess": isSuccess,
	})
}

// 更新站点信息
func (c *ConfigWebSet) UpdateInfo(context *gin.Context) {
	var content *webset_model.WebSet
	err := context.ShouldBind(&content)
	var isSuccess bool
	if err != nil {
		isSuccess = false
	}

	isSuccess = webset_service.UpdateInfo(content.WebContent, content.WebId)
	context.JSON(http.StatusOK, gin.H{
		"data": isSuccess,
	})
}
