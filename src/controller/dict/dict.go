package configdict

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"yooome/model/sys_dict_type"
	"yooome/service/dict_service"
	"yooome/service/userinfo_service"
)

type ConfigDict struct {
}

// 根据分页请求参数获取分页列表
func (c *ConfigDict) GetDictList(context *gin.Context) {
	var pageReq sys_dict_type.PageReqParams
	if err := context.ShouldBind(&pageReq); err != nil {
		fmt.Println("context should bind error ", err)
		return
	}
	// 进行分页查询
	total, page, list, err := dict_service.GetDictListByPage(&pageReq)
	if err != nil {
		fmt.Println("get dict list by page err", err)
		return
	}
	// 菜单 正常 or 停用 状态
	statusList, err := dict_service.GetStatusByDictType("sys_normal_disable", "", "")
	if err != nil {
		logrus.Error("get status list error", err)
		return
	}
	result := map[string]interface{}{
		"currentPage":  page,
		"total":        total,
		"list":         list,
		"searchStatus": statusList,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": result,
	})
}

// 添加字典
func (c *ConfigDict) AddDict(context *gin.Context) {
	var addDictType *sys_dict_type.AddDictType
	err := context.ShouldBind(&addDictType)
	if err != nil {
		logrus.Error("context should bind addDictType error ", err)
		return
	}
	// 获取用户的 id
	userId := userinfo_service.GetLoginID(context)
	// 保存添加的字典数据
	code, err := dict_service.AddDict(addDictType, userId)
	if err != nil {
		logrus.Error("add dict error", err)
	}
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": code,
	})
}

// @Summary 根据字典 dictId 获取 详细信息
func (c *ConfigDict) GetDetail(context *gin.Context) {
	// 获取传递过来的  dictId
	dictId := context.Query("dictId")
	if dictId == "" {
		return
	}
	dict, err := dict_service.GetDetail(dictId)
	if err != nil {
		logrus.Error("can not find dict by id")
		return
	}
	context.JSON(http.StatusOK, gin.H{
		"data": dict,
	})
}

// 修改字典管理
func (d *ConfigDict) UpdateDict(context *gin.Context) {
	var updateDict *sys_dict_type.UpdateDictType
	err := context.ShouldBind(&updateDict)
	if err != nil {
		logrus.Error("context should bind updateDict error ", err)
		return
	}
	// 获取用户的 id
	userId := userinfo_service.GetLoginID(context)
	// 修改的字典数据
	code, err := dict_service.UpdateDict(updateDict, userId)
	if err != nil {
		logrus.Error("add dict error", err)
	}
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": code,
	})
}

// 删除字典数据
func (d *ConfigDict) DeleteByDictId(context *gin.Context) {
	var dictIds []int
	str := context.ShouldBindJSON(&dictIds)
	if dictIds == nil || str != nil {
		logrus.Error("context should bind dictId error ")
		return
	}
	isSuccess := dict_service.DeleteByDictId(dictIds)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}
