package cron

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/host"
	"github.com/shirou/gopsutil/v3/load"
	"github.com/shirou/gopsutil/v3/mem"
	"net/http"
	"os"
	"runtime"
	"time"
	"yooome/model/job_params"
	"yooome/model/loginlog_params"
	"yooome/service/cronjob_service"
	"yooome/service/dict_service"
	"yooome/service/loginlog_service"
	"yooome/service/userinfo_service"
	"yooome/utils/convert"
	"yooome/utils/tools"
	"yooome/utils/ytime"
)

type CronJob struct {
}

var StartTime = ytime.DateTime()

func (c *CronJob) GetJobList(context *gin.Context) {
	var params job_params.QueryJobParams
	err := context.ShouldBind(&params)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	total, page, list, err := cronjob_service.GetJobList(&params)
	// 获取相关的选项
	jobStatus, err := dict_service.GetStatusByDictType("sys_job_status", "", "")
	if err != nil {
		isSuccess = false
	}
	jobGroup, err := dict_service.GetStatusByDictType("sys_job_group", "", "")
	if err != nil {
		isSuccess = false
	}
	policy, err := dict_service.GetStatusByDictType("sys_job_policy", "", "")
	if err != nil {
		isSuccess = false
	}
	result := map[string]interface{}{
		"currentPage":      page,
		"total":            total,
		"list":             list,
		"searchStatus":     jobStatus,
		"searchGroup":      jobGroup,
		"isSuccess":        isSuccess,
		"jobPolicyOptions": policy,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": result,
	})
}

// 添加任务
func (c *CronJob) AddJob(context *gin.Context) {
	var params job_params.AddJobParams
	isSuccess := true
	err := context.ShouldBindJSON(&params)
	if err != nil {
		isSuccess = false
	}
	// 获取用户的 id
	userId := userinfo_service.GetLoginID(context)
	isSuccess = cronjob_service.AddJob(&params, userId)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 根据id查询任务id
func (c *CronJob) GetJobByList(context *gin.Context) {
	var id string
	id = context.Query("id")
	job, err := cronjob_service.GetJobById(id)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	context.JSON(http.StatusOK, gin.H{
		"data":      job,
		"isSuccess": isSuccess,
	})
}

// 修改任务
func (c *CronJob) UpdateJob(context *gin.Context) {
	var params job_params.UpdateJobParams
	isSuccess := true
	err := context.ShouldBindJSON(&params)
	if err != nil {
		isSuccess = false
	}
	// 获取用户的 id
	userId := userinfo_service.GetLoginID(context)
	isSuccess = cronjob_service.UpdateJob(&params, int(userId))
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 根据id 删除任务id
func (c *CronJob) DelJobById(context *gin.Context) {
	var id string
	id = context.Query("id")
	isSuccess := cronjob_service.DelJobById(id)
	context.JSON(http.StatusOK, gin.H{
		"isSuccess": isSuccess,
	})
}

// 获取计算机的cpu信息
func (c *CronJob) GetComputerInfo(context *gin.Context) {
	// 获取计算机的核心数
	cpuNum := runtime.NumCPU()
	// 用户使用率
	var cpuUsed float64 = 0
	// cpu负载5
	var cpuAvg5 float64 = 0
	// 当前空闲率
	var cpuAvg15 float64 = 0
	cpuInfo, err := cpu.Percent(time.Duration(time.Second), false)
	if err == nil {
		cpuUsed = convert.Float64(fmt.Sprintf("%.2f", cpuInfo[0]))
	}
	loadInfo, err := load.Avg()
	if err == nil {
		cpuAvg5 = convert.Float64(fmt.Sprintf("%.2f", loadInfo.Load5))
		cpuAvg15 = convert.Float64(fmt.Sprintf("%.2f", loadInfo.Load5))
	}
	var memTotal uint64 = 0  // 总内存
	var memUsed uint64 = 0   // 已用内存
	var memFree uint64 = 0   // 剩余内存
	var memUsage float64 = 0 // 使用率

	v, err := mem.VirtualMemory()
	if err == nil {
		memTotal = v.Total / 1024 / 1024
		memUsed = v.Used / 1024 / 1024
		memFree = memTotal - memUsed
		memUsage = convert.Float64(fmt.Sprintf("%.2f", v.UsedPercent))

	}
	var goTotal uint64 = 0  // go 分配的总内存
	var goUsed uint64 = 0   // go使用的内存数
	var goFree uint64 = 0   // go剩余的内存数
	var goUsage float64 = 0 // 使用率

	var gomen runtime.MemStats
	runtime.ReadMemStats(&gomen)
	goUsed = gomen.Sys / 1024 / 1024
	goUsage = convert.Float64(fmt.Sprintf("%.2f", convert.Float64(goUsed)/convert.Float64(memTotal)*100))
	sysComputerIp := "" // 服务ip
	ip, err := tools.GetLocalIP()
	if err == nil {
		sysComputerIp = ip
	}
	sysComputerName := "" //服务器名称
	sysOsName := ""       // 操作系统
	sysOsArch := ""       // 系统架构
	sysInfo, err := host.Info()
	if err == nil {
		sysComputerName = sysInfo.Hostname
		sysOsName = sysInfo.OS
		sysOsArch = sysInfo.KernelArch
	}
	goName := "Golang"             // 语言环境
	goVersion := runtime.Version() // 版本
	ytime.Date()
	goStartTime := StartTime // 启动时间

	GoRunTime := tools.GetHourDiffer(StartTime, ytime.DateTime())
	goHome := runtime.GOROOT()

	goUserDir := ""

	curDir, err := os.Getwd()
	if err == nil {
		goUserDir = curDir
	}
	// 服务器磁盘信息
	diskList := make([]disk.UsageStat, 0)
	diskInfo, err := disk.Partitions(true)
	if err == nil {
		for _, p := range diskInfo {
			diskDetail, err := disk.Usage(p.Mountpoint)
			if err == nil {
				diskDetail.UsedPercent = convert.Float64(fmt.Sprintf("%.2f", diskDetail.UsedPercent))
				diskDetail.Total = diskDetail.Total / 1024 / 1024
				diskDetail.Used = diskDetail.Used / 1024 / 1024
				diskDetail.Free = diskDetail.Free / 1024 / 1024
				diskList = append(diskList, *diskDetail)
			}
		}
	}
	data := map[string]interface{}{
		"cpuNum":          cpuNum,
		"cpuUsed":         cpuUsed,
		"cpuAvg5":         cpuAvg5,
		"cpuAvg15":        cpuAvg15,
		"memTotal":        memTotal,
		"goTotal":         goTotal,
		"memUsed":         memUsed,
		"goUsed":          goUsed,
		"memFree":         memFree,
		"goFree":          goFree,
		"memUsage":        memUsage,
		"goUsage":         goUsage,
		"sysComputerName": sysComputerName,
		"sysOsName":       sysOsName,
		"sysComputerIp":   sysComputerIp,
		"sysOsArch":       sysOsArch,
		"goName":          goName,
		"goVersion":       goVersion,
		"goStartTime":     goStartTime,
		"goRunTime":       GoRunTime,
		"goUserDir":       goUserDir,
		"disklist":        diskList,
		"goHome":          goHome,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 获取登录日志记录
func (c *CronJob) GetLoginLogList(context *gin.Context) {
	var params loginlog_params.GetLoginLogParams
	err := context.ShouldBind(&params)
	isSuccess := true
	if err != nil && &params != nil {
		isSuccess = false
	}
	total, page, list, err := loginlog_service.GetLoginLogList(&params)
	// 获取相关的选项
	logStatus, err := dict_service.GetStatusByDictType("admin_login_status", "", "")
	if err != nil {
		isSuccess = false
	}
	data := map[string]interface{}{
		"currentPage":  page,
		"total":        total,
		"list":         list,
		"searchStatus": logStatus,
		"isSuccess":    isSuccess,
	}
	context.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

// 删除登录日志
func (c *CronJob) DelLoginLog(context *gin.Context) {
	var ids []int
	err := context.ShouldBindJSON(&ids)
	isSuccess := true
	if err != nil {
		isSuccess = false
	}
	isSuccess = loginlog_service.DelLoginLog(ids)
	context.JSON(http.StatusOK, gin.H{
		"data": isSuccess,
	})
}
