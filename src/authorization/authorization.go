package authorization

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type HeaderParams struct {
	Authorization string `header:"Authorization"`
}

func CheckTokenAuth() gin.HandlerFunc {
	return func(context *gin.Context) {
		// 认证拦截路径是否合法
		isPath := AuthPath(context)
		if !isPath {
			context.Next()
			return
		}
		headerParams := HeaderParams{}
		if err := context.ShouldBindHeader(&headerParams); err != nil {
			logrus.Error("context should bind header err = ", err)
			context.Abort()
		}
		if len(headerParams.Authorization) >= 20 {
			fmt.Println("check token auth test....")
		}
	}
}

// 验证拦截的路径是否和法
func AuthPath(context *gin.Context) bool {
	path := context.FullPath()
	loginPath := "/admin/users/login"
	if path == loginPath {
		return false
	}
	return true
}
