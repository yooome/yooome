package cache

import (
	"time"
)

// Remove deletes one or more keys from cache, and returns its value.
// If multiple keys are given, it returns the value of the last deleted item.
func (c *Cache) Remove(keys ...interface{}) (value interface{}, err error) {
	return c.adapter.Remove(c.getCtx(), keys...)
}

// Get retrieves and returns the associated value of given <key>.
// It returns nil if it does not exist, its value is nil or it's expired.
func (c *Cache) Get(key interface{}) (interface{}, error) {
	return c.adapter.Get(c.getCtx(), key)
}
func (c *Cache) SetIfNotExist(key interface{}, value interface{}, duration time.Duration) (bool, error) {
	return c.adapter.SetIfNotExist(c.getCtx(), key, value, duration)
}
func (c *Cache) Set(key interface{}, value interface{}, duration time.Duration) error {
	return c.adapter.Set(c.getCtx(), key, value, duration)
}
func (c *Cache) Sets(data map[interface{}]interface{}, duration time.Duration) error {
	return c.adapter.Sets(c.getCtx(), data, duration)
}
func (c *Cache) GetOrSet(key interface{}, value interface{}, duration time.Duration) (interface{}, error) {
	return c.adapter.GetOrSet(c.getCtx(), key, value, duration)
}
func (c *Cache) GetOrSetFunc(key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error) {
	return c.adapter.GetOrSetFun(c.getCtx(), key, f, duration)
}
func (c *Cache) GetOrSetFuncLock(key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error) {
	return c.adapter.GetOrSetFuncLock(c.getCtx(), key, f, duration)
}
func (c *Cache) Contains(key interface{}) (bool, error) {
	return c.adapter.Contains(c.getCtx(), key)
}
func (c *Cache) GetExpire(key interface{}) (time.Duration, error) {
	return c.adapter.GetExpire(c.getCtx(), key)
}

func (c *Cache) Update(key interface{}, value interface{}) (oldValue interface{}, exist bool, err error) {
	return c.adapter.Update(c.getCtx(), key, value)
}
func (c *Cache) UpdateExpire(key interface{}, duration time.Duration) (oldDuration time.Duration, err error) {
	return c.adapter.UpdateExpire(c.getCtx(), key, duration)
}
func (c *Cache) Size() (size int, err error) {
	return c.adapter.Size(c.getCtx())
}
func (c *Cache) Data() (map[interface{}]interface{}, error) {
	return c.adapter.Data(c.getCtx())
}
func (c *Cache) Keys() ([]interface{}, error) {
	return c.adapter.Keys(c.getCtx())
}
func (c *Cache) Values() ([]interface{}, error) {
	return c.adapter.Values(c.getCtx())
}
func (c *Cache) Clear() error {
	return c.adapter.Clear(c.getCtx())
}
func (c *Cache) Close() error {
	return c.adapter.Close(c.getCtx())
}
