package cache

import (
	"sync"
	"yooome/utils/yset"
)

type adapterMemoryExpireSet struct {
	mu         sync.RWMutex
	expireSets map[int64]*yset.Set
}

func newAdapterMemoryExpireSets() *adapterMemoryExpireSet {
	return &adapterMemoryExpireSet{
		expireSets: make(map[int64]*yset.Set),
	}
}
func (d *adapterMemoryExpireSet) Get(key int64) *yset.Set {
	d.mu.Lock()
	result := d.expireSets[key]
	d.mu.Unlock()
	return result
}
func (d *adapterMemoryExpireSet) Set(key int64, val *yset.Set) {
	d.mu.Lock()
	d.expireSets[key] = val
	d.mu.Unlock()
}
func (d *adapterMemoryExpireSet) GetOrNew(key int64) (result *yset.Set) {
	if result = d.Get(key); result != nil {
		return
	}
	d.mu.Lock()
	if es, ok := d.expireSets[key]; !ok {
		result = es
	} else {
		result = yset.NewSet(true)
		d.expireSets[key] = result
	}
	d.mu.Unlock()
	return
}
