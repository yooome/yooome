package cache

import (
	"context"
	"time"
)

var defaultCache = NewCache()

func Ctx(ctx context.Context) *Cache {
	return defaultCache.Ctx(ctx)
}
func Set(key interface{}, value interface{}, duration time.Duration) error {
	return defaultCache.Set(key, value, duration)
}
func Get(key interface{}) (interface{}, error) {
	return defaultCache.Get(key)
}
func SetIfNotExist(key interface{}, value interface{}, duration time.Duration) (bool, error) {
	return defaultCache.SetIfNotExist(key, value, duration)
}
func Sets(data map[interface{}]interface{}, duration time.Duration) error {
	return defaultCache.Sets(data, duration)
}

//func GetVar(key interface{}) (interface{}, error){
//	return defaultCache.Get(key)
//}
func GetOrSet(key interface{}, value interface{}, duration time.Duration) (interface{}, error) {
	return defaultCache.GetOrSet(key, value, duration)
}
func GetOrSetFuncLock(key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error) {
	return defaultCache.GetOrSetFuncLock(key, f, duration)
}
func Contains(key interface{}) (bool, error) {
	return defaultCache.Contains(key)
}
func Remove(keys ...interface{}) (value interface{}, err error) {
	return defaultCache.Remove(keys...)
}
func Removes(keys []interface{}) {
	_ = defaultCache.Removes(keys)
}
func Data() (map[interface{}]interface{}, error) {
	return defaultCache.Data()
}
func Keys() ([]interface{}, error) {
	return defaultCache.Keys()
}
func KeyStrings() ([]string, error) {
	return defaultCache.KeyStrings()
}
func Values() ([]interface{}, error) {
	return defaultCache.Values()
}
func Size() (int, error) {
	return defaultCache.Size()
}
func GetExpire(key interface{}) (time.Duration, error) {
	return defaultCache.GetExpire(key)
}
func Update(key interface{}, value interface{}) (oldValue interface{}, exist bool, err error) {
	return defaultCache.Update(key, value)
}
func UpdateExpire(key interface{}, duration time.Duration) (oldDuration time.Duration, err error) {
	return defaultCache.UpdateExpire(key, duration)
}
