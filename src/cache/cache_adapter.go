package cache

import (
	"context"
	"time"
)

type Adapter interface {
	// set
	Set(ctx context.Context, key interface{}, value interface{}, duration time.Duration) error
	// Sets batch sets cache with key-value pairs by<data> which is expired after <duration>.
	//
	// It does not expired if <duration> == 0
	// It deletes the keys of <data> if <duration> < 0 or give
	Sets(ctx context.Context, data map[interface{}]interface{}, duration time.Duration) error
	//
	SetIfNotExist(ctx context.Context, key interface{}, value interface{}, duration time.Duration) (bool, error)
	//
	Get(context context.Context, key interface{}) (interface{}, error)
	//
	GetOrSet(ctx context.Context, key interface{}, value interface{}, duration time.Duration) (interface{}, error)
	//
	GetOrSetFun(ctx context.Context, key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error)
	//
	GetOrSetFuncLock(ctx context.Context, key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error)
	//
	Contains(ctx context.Context, key interface{}) (bool, error)
	//
	GetExpire(ctx context.Context, key interface{}) (time.Duration, error)
	//
	Remove(ctx context.Context, keys ...interface{}) (value interface{}, err error)
	//
	Update(ctx context.Context, key interface{}, value interface{}) (oldValue interface{}, exist bool, err error)
	//
	UpdateExpire(ctx context.Context, key interface{}, duration time.Duration) (oldDuration time.Duration, err error)
	//
	Size(ctx context.Context) (size int, err error)
	//
	Data(ctx context.Context) (map[interface{}]interface{}, error)
	// Keys return all keys in the cache as slice
	Keys(ctx context.Context) ([]interface{}, error)
	//
	Values(ctx context.Context) ([]interface{}, error)
	//
	Clear(ctx context.Context) error
	//
	Close(ctx context.Context) error
}
