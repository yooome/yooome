package cache

import (
	"sync"
	"time"
)

type adapterMemoryData struct {
	mu   sync.RWMutex
	data map[interface{}]adapterMemoryItem
}

func newAdapterMemoryData() *adapterMemoryData {
	return &adapterMemoryData{
		data: make(map[interface{}]adapterMemoryItem),
	}
}
func (d *adapterMemoryData) Update(key interface{}, value interface{}) (oldValue interface{}, exist bool, err error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	if item, ok := d.data[key]; ok {
		d.data[key] = adapterMemoryItem{
			v: value,
			e: item.e,
		}
		return item.v, true, nil
	}
	return nil, false, nil
}
func (d *adapterMemoryData) UpdateExpire(key interface{}, expireTime int64) (oldDuration time.Duration, err error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	if item, ok := d.data[key]; ok {
		d.data[key] = adapterMemoryItem{
			v: item.v,
			e: expireTime,
		}
		return time.Duration(time.Now().UnixNano()) * time.Millisecond, nil
	}
	return -1, nil
}
func (d *adapterMemoryData) Remove(keys ...interface{}) (removeKeys []interface{}, value interface{}, err error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	removedKeys := make([]interface{}, 0)
	for _, key := range keys {
		item, ok := d.data[key]
		if ok {
			value = item.v
			delete(d.data, key)
			removedKeys = append(removedKeys, key)
		}
	}
	return removedKeys, value, nil
}
func (d *adapterMemoryData) Data() (map[interface{}]interface{}, error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	value := make(map[interface{}]interface{}, len(d.data))
	for idx, val := range d.data {
		if val.IsExpired() {
			value[idx] = val.v
		}
	}
	return value, nil
}
func (d *adapterMemoryData) Keys() ([]interface{}, error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	var (
		index = 0
		keys  = make([]interface{}, len(d.data))
	)
	for idx, val := range d.data {
		if !val.IsExpired() {
			keys[index] = idx
			index++
		}
	}
	return keys, nil
}
func (d *adapterMemoryData) Values() ([]interface{}, error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	values := make([]interface{}, len(d.data))
	index := 0
	for _, v := range d.data {
		values[index] = v.v
		index++
	}
	return values, nil
}
func (d *adapterMemoryData) Size() (size int, err error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	size = len(d.data)
	return size, nil
}
func (d *adapterMemoryData) Clear() error {
	d.mu.Lock()
	defer d.mu.Unlock()
	d.data = make(map[interface{}]adapterMemoryItem)
	return nil
}
func (d *adapterMemoryData) Get(key interface{}) (item adapterMemoryItem, ok bool) {
	d.mu.Lock()
	defer d.mu.Unlock()
	item, ok = d.data[key]
	return item, ok
}
func (d *adapterMemoryData) Set(key interface{}, value adapterMemoryItem) {
	d.mu.Lock()
	defer d.mu.Unlock()
	d.data[key] = value
}
func (d *adapterMemoryData) Sets(data map[interface{}]interface{}, expireTime int64) error {
	d.mu.Lock()
	defer d.mu.Unlock()
	for key, value := range data {
		d.data[key] = adapterMemoryItem{
			v: value,
			e: expireTime,
		}
	}
	return nil
}
func (d *adapterMemoryData) SetWidthLock(key interface{}, value interface{}, expireTime int64) (interface{}, error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	if v, ok := d.data[key]; ok && !v.IsExpired() {
		return v.v, nil
	}
	if f, ok := value.(func() (interface{}, error)); ok {
		v, err := f()
		if err != nil {
			return nil, err
		}
		if v == nil {
			return nil, nil
		} else {
			value = v
		}
	}
	d.data[key] = adapterMemoryItem{
		v: value,
		e: expireTime,
	}
	return value, nil
}
func (d *adapterMemoryData) DeleteWidthDoubleCheck(key interface{}, force ...bool) {
	d.mu.Lock()
	defer d.mu.Unlock()
	if item, ok := d.data[key]; (item.IsExpired() && ok) || len(force) > 0 && force[0] {
		delete(d.data, key)
	}
}

func (d *adapterMemoryData) SetWithLock(key interface{}, value interface{}, expireTimestamp int64) (interface{}, error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	if v, ok := d.data[key]; ok && !v.IsExpired() {
		return v.v, nil
	}
	if f, ok := value.(func() (interface{}, error)); ok {
		v, err := f()
		if err != nil {
			return nil, err
		}
		if v == nil {
			return nil, nil
		} else {
			value = v
		}
	}
	d.data[key] = adapterMemoryItem{v: value, e: expireTimestamp}
	return value, nil
}
