package sys_config_params

// 查询差数列表结构体
type ParamsPageReq struct {
	PageNum    int    `form:"pageNum" json:"pageNum"`
	PageSize   int    `form:"pageSize" json:"pageSize"`
	ConfigName string `form:"configName" json:"configName"`
	ConfigKey  string `form:"configKey" json:"configKey"`
	ConfigType string `form:"configType" json:"configType"`
	BeginTime  string `form:"beginTime" json:"beginTime"`
	EndTime    string `form:"endTime" json:"endTime"`
}

// 添加参数列表结构体
type AddParamsReq struct {
	ConfigName  string `form:"configName" json:"configName"`
	ConfigKey   string `form:"configKey" json:"configKey"`
	ConfigValue string `form:"configValue" json:"configValue"`
	ConfigType  int    `form:"configType" json:"configType"`
	Remark      string `form:"remark" json:"remark"`
}

// 修改参数列表的结构体
type UpdParamsReq struct {
	ConfigId    string `form:"configId" json:"configId"`
	ConfigName  string `form:"configName" json:"configName"`
	ConfigKey   string `form:"configKey" json:"configKey"`
	ConfigValue string `form:"configValue" json:"configValue"`
	ConfigType  int    `form:"configType" json:"configType"`
	Remark      string `form:"remark" json:"remark"`
}
