package modelinfo_model

import (
	"errors"
	"yooome/model"
	"yooome/model/cmsmenu_model"
	"yooome/model/cmsmenu_params"
	"yooome/utils/convert"
)

type ModelInfo struct {
	model.BaseModel `json:"-"`
	ModelId         int    `gorm:"model_id" json:"modelId"`
	ModelCategoryId int    `gorm:"model_category_id" json:"modelCategoryId"`
	ModelName       string `gorm:"model_name" json:"modelName"`
	ModelTitle      string `gorm:"model_title" json:"modelTitle"`
	ModelPk         string `gorm:"model_pk" json:"modelPk"`
	ModelOrder      string `gorm:"model_order" json:"modelOrder"`
	ModelSort       string `gorm:"model_sort" json:"modelSort"`
	ModelList       string `gorm:"model_list" json:"modelList"`
	ModelEdit       string `gorm:"model_edit" json:"modelEdit"`
	ModelIndexes    string `gorm:"model_indexes" json:"modelIndexes"`
	SearchList      string `gorm:"search_list" json:"searchList"`
	CreateTime      int    `gorm:"create_time" json:"createTime"`
	UpdateTime      int    `gorm:"update_time" json:"updateTime"`
	ModelStatus     int    `gorm:"model_status" json:"modelStatus"`
	ModelEngine     string `gorm:"model_engine" json:"modelEngine"`
	CreateBy        int    `gorm:"create_by" json:"createBy"`
	UpdateBy        int    `gorm:"update_by" json:"updateBy"`
}

func NewModelInfoFactory(sqlType string) *ModelInfo {
	return &ModelInfo{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (m *ModelInfo) TableName() string {
	return "model_info"
}
func (m *ModelInfo) GetModelByCateIds(cateIds []int64) (mode []*ModelInfo, err error) {
	sql := "select model_id,model_name,model_title from model_info where model_category_id in (?)"
	db := m.DB.Raw(sql, cateIds).Order("model_id asc").Find(&mode)
	if db.RowsAffected > 0 {
		return mode, nil
	} else {
		return nil, errors.New("get model info error")
	}
}
func (m *ModelInfo) AddModelOptions(params *cmsmenu_params.AddMenuParams) bool {
	parentId := params.ParentId
	modelId := params.ModelId
	cateType := params.CateType
	status := params.Status
	name := params.Name
	alias := params.Alias
	description := params.Description
	seoTitle := params.InputSeoTitle
	seoKeywords := params.InputSeoKeywords
	seoDescription := params.InputSeoDescription
	cateAddress := params.CateAddress
	cateContent := params.CateContent
	listTemplate := params.ListTemplate
	contentTemplate := params.ContentTemplate
	sql := "insert into cms_category (parent_id,model_id,cate_type,status,name,alias,description,seo_title,seo_keywords,seo_description,cate_address,cate_content," +
		"list_template,content_template) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	db := m.DB.Exec(sql, parentId, modelId, convert.Int64(cateType), convert.Int64(status), name, alias, description, seoTitle, seoKeywords, seoDescription, cateAddress, cateContent, listTemplate, contentTemplate)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (m *ModelInfo) GetMenuById(id string) (mode *cmsmenu_model.CmsMenuModel, err error) {
	sql := "select * from cms_category where id in (?)"
	db := m.DB.Raw(sql, convert.Int64(id)).Find(&mode)
	if db.RowsAffected > 0 {
		return mode, nil
	} else {
		return nil, errors.New("get category error")
	}
}
func (m *ModelInfo) GetMenuListChannel() (mode []*cmsmenu_model.CmsMenuModel, err error) {
	sql := "select * from cms_category"
	db := m.DB.Raw(sql).Find(&mode).Order("list_order asc, id asc")
	if db.RowsAffected > 0 {
		return mode, nil
	} else {
		return nil, errors.New("get category error")
	}
}
func (m *ModelInfo) UpdateCmsCategory(params *cmsmenu_params.UpdateMenuParams) bool {
	id := params.Id
	parentId := params.ParentId
	modelId := params.ModelId
	cateType := params.CateType
	status := params.Status
	name := params.Name
	alias := params.Alias
	description := params.Description
	seoTitle := params.InputSeoTitle
	seoKeywords := params.InputSeoKeywords
	seoDescription := params.InputSeoDescription
	cateAddress := params.CateAddress
	cateContent := params.CateContent
	listTemplate := params.ListTemplate
	contentTemplate := params.ContentTemplate
	sql := "update cms_category set parent_id=?,model_id=?,cate_type=?,status=?,name=?,alias=?,description=?,seo_title=?,seo_keywords=?," +
		"seo_description=?,cate_address=?,cate_content=?,list_template=?,content_template=? where id=?"
	db := m.DB.Exec(sql, parentId, modelId, cateType, status, name, alias, description, seoTitle, seoKeywords, seoDescription, cateAddress, cateContent, listTemplate, contentTemplate, id)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}

// 删除 category
func (m *ModelInfo) DeleteCategory(ids []int64) bool {
	sql := "delete from cms_category where id in (?)"
	db := m.DB.Exec(sql, ids)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
