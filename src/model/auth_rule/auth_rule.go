package auth_rule

import (
	"errors"
	"fmt"
	"yooome/model"
	"yooome/model/auth_model"
	"yooome/utils/convert"
	"yooome/utils/ytime"
)

type AuthRule struct {
	model.BaseModel `json:"-"`
	Id              int64  `gorm:"id" json:"id"`
	CreateTime      int64  `gorm:"create_time" json:"create_time"`
	UpdateTime      int64  `gorm:"update_time" json:"update_time"`
	Pid             uint   `json:"pid" gorm:"pid"`                // 父ID
	Name            string `json:"name" gorm:"name"`              // 规则名称
	Title           string `json:"title" gorm:"title"`            // 规则名称
	Icon            string `json:"icon" gorm:"icon"`              // 图标
	Condition       string `json:"condition" gorm:"condition"`    // 条件
	Remark          string `json:"remark" gorm:"remark"`          // 备注
	MenuType        uint   `json:"menuType" gorm:"menu_type"`     // 类型 0目录，1菜单，2按钮
	Weigh           int    `json:"weigh" gorm:"weigh"`            // 权重
	Status          uint   `json:"status" gorm:"status"`          // 状态
	AlwaysShow      uint   `json:"AlwaysShow" gorm:"always_show"` // 显示状态
	Path            string `json:"path" gorm:"path"`              // 路由地址
	Component       string `json:"component" gorm:"component"`    // 组件路径
	IsFrame         uint   `json:"isFrame" gorm:"is_frame"`       // 是否外连接 1是，2否
	ModuleType      string `json:"moduleType" gorm:"module_type"` // 所属模块
	ModelId         uint   `json:"modelId" gorm:"model_id"`       // 模型ID
}

// 创建连接 AuthRule 连接数据库工厂
func NewAuthRuleFactory(sqlType string) *AuthRule {
	return &AuthRule{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}

}
func (a *AuthRule) TableName() string {
	return "auth_rule"
}

func (a *AuthRule) GetMenuList() (list []*AuthRule, err error) {
	// TODO 从缓存中获取
	sql := "select * from auth_rule"
	// 从数据库中获取数据
	result := a.Raw(sql).Find(&list)
	fmt.Println("find result = ", result)
	// 设置数据到缓存
	return list, nil
}
func (a *AuthRule) GetMenuListByParams(queryParams *auth_model.GetMenuListParams) (list []*AuthRule, err error) {
	db := a.DB.Find(&list)
	if queryParams != nil {
		if queryParams.MenuName != "" {
			db = db.Where("title like ?", "%"+queryParams.MenuName+"%").Find(&list)
		}
		if queryParams.Status != "" {
			db = db.Where("status = ?", convert.Int64(queryParams.Status)).Find(&list)
		}
	}
	if db.RowsAffected > 0 {
		return list, nil
	} else {
		return nil, errors.New("can not get menu list")
	}
}
func (a *AuthRule) GetMenuDetailById(menuId string) (menuDetail *AuthRule, err error) {
	db := a.DB.Where("id = ?", convert.Int64(menuId)).Find(&menuDetail)
	if db.RowsAffected > 0 {
		return menuDetail, nil
	} else {
		return nil, errors.New("can not get menu list")
	}
}
func (a *AuthRule) CheckMenuNameUnique(name string, id int) bool {
	var menu *AuthRule
	db := a.DB.Where("name = ?", name).Find(&menu)
	if id != 0 {
		db = db.Where("id = ?", id).Find(&menu)
	}
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (a *AuthRule) CheckMenuPathUnique(path string, id int) bool {
	var menu *AuthRule
	db := a.DB.Where("path = ?", path).Find(&menu)
	if id != 0 {
		db = db.Where("id = ?", id).Find(&menu)
	}
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (a *AuthRule) UpdateMenuById(menus *auth_model.UpdateMenuParams) bool {
	sql := "update auth_rule set pid = ?, name = ?, title = ?, icon = ?, menu_type = ?, updatetime = ?, weigh = ?," +
		"status = ?, always_show = ?, path = ?, component = ?, is_frame = ? where id = ?"
	pid := menus.Pid
	name := menus.Name
	title := menus.Title
	icon := menus.Icon
	menu_type := menus.MenuType
	updateTime := convert.Uint64(ytime.Timestamp())
	weigh := menus.Weigh
	status := menus.Status
	alwaysShow := menus.Always_Show
	path := menus.Path
	component := menus.Component
	isFrame := menus.IsFrame
	id := menus.MenuId
	db := a.DB.Exec(sql, pid, name, title, icon, menu_type, updateTime, weigh, status, alwaysShow, path, component, isFrame, id)

	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (a *AuthRule) AddMenu(addMenu *auth_model.AddMenuParams) bool {
	sql := "insert into auth_rule (title,status,menu_type,path,component,always_show,name,icon,is_frame,createtime,updatetime,weigh,pid)" +
		"values(?,?,?,?,?,?,?,?,?,?,?,?,?)"

	title := addMenu.Title
	status := addMenu.Status
	menu_type := addMenu.MenuType
	path := addMenu.Path
	component := addMenu.Component
	alwaysShow := addMenu.Always_Show
	name := addMenu.Name
	icon := addMenu.Icon
	isFrame := addMenu.IsFrame
	createTime := convert.Uint64(ytime.Timestamp())
	updateTime := convert.Uint64(ytime.Timestamp())
	weigh := addMenu.Weigh
	pid := addMenu.Pid
	db := a.DB.Exec(sql, title, status, menu_type, path, component, alwaysShow, name, icon, isFrame, createTime, updateTime, weigh, pid)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (a *AuthRule) DeleteMenu(id int) bool {
	sql := "delete from auth_rule where id = ?"
	db := a.DB.Exec(sql, id)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
