package loginlog_model

import (
	"yooome/model"
	"yooome/model/loginlog_params"
	"yooome/utils/convert"
	"yooome/utils/ytime"
)

type LoginLogModel struct {
	model.BaseModel `json:"-"`
	InfoId          int64  `gorm:"info_id" json:"infoId"`
	LoginName       string `gorm:"login_name" json:"loginName"`
	Ipaddr          string `gorm:"ipaddr" json:"ipaddr"`
	LoginLocation   string `gorm:"login_location" json:"loginLocation"`
	Browser         string `gorm:"browser" json:"browser"`
	Os              string `gorm:"os" json:"os"`
	Status          int64  `gorm:"status" json:"status"`
	Msg             string `gorm:"msg" json:"msg"`
	LoginTime       int64  `gorm:"login_time" json:"loginTime"`
	Module          string `gorm:"module" json:"module"`
}

func NewLoginLogFactory(sqlType string) *LoginLogModel {
	return &LoginLogModel{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (l *LoginLogModel) TableName() string {
	return "sys_login_log"
}
func (l *LoginLogModel) GetLoginLogList(params *loginlog_params.GetLoginLogParams) (total, page int, list []*LoginLogModel, err error) {
	if params.PageNum == 0 {
		params.PageNum = 1
	}
	if params.PageSize == 0 {
		params.PageSize = 10
	}
	order := "info_id desc"
	db := l.DB.Find(&list)
	if params.LoginName != "" {
		db = db.Where("login_name like ?", "%"+params.LoginName+"%")
	}
	if params.Status != "" {
		db = db.Where("status = ?", convert.Int64(params.Status))
	}
	if params.BeginTime != "" {
		tm, _ := ytime.StrToTime(params.BeginTime)
		db = db.Where("login_time >=", tm.Timestamp())
	}
	if params.SortName != "" {
		if params.SortOrder != "" {
			order = params.SortName + " " + params.SortOrder
		} else {
			order = params.SortName + " DESC"
		}
	}
	total = len(list)
	if params.PageNum > 0 && params.PageSize > 0 {
		res := (params.PageNum - 1) * params.PageSize
		db.Limit(params.PageSize).Offset(res).Order(order).Find(&list)
	}
	return total, params.PageNum, list, nil

}

// 删除日志
func (l *LoginLogModel) DelLoginLog(ids []int) bool {
	sql := "delete from sys_login_log where info_id in (?)"
	db := l.DB.Exec(sql, ids)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
