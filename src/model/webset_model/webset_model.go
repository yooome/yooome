package webset_model

import (
	"yooome/model"
)

type WebSet struct {
	model.BaseModel `json:"-" form:"-"`
	WebId           int    `gorm:"web_id" form:"webId" json:"webId"`
	WebContent      string `gorm:"web_content" form:"webContent" json:"webContent"`
}

// 创建webset 工厂
func NewWebSetFactory() *WebSet {
	return &WebSet{BaseModel: model.BaseModel{DB: model.DBConn("")}}
}
func (w *WebSet) TableName() string {
	return "web_set"
}

// 查询 webset 数据
func (w *WebSet) GetInfo() (webSet *WebSet) {
	sql := "select * from web_set"
	res := w.DB.Exec(sql).Find(&webSet)
	if res.RowsAffected > 0 {
		return webSet
	} else {
		return nil
	}
}

func (w *WebSet) UpdateInfo(content string, id int) bool {
	sql := "update web_set set web_content = ? where web_id = ?"
	res := w.DB.Exec(sql, content, id)
	if res.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
