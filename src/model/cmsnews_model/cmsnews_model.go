package cmsnews_model

import (
	"yooome/model"
	"yooome/model/cmsmenu_model"
	"yooome/model/cmsmenu_params"
	"yooome/utils/convert"
	"yooome/utils/slicetree"
)

type NewModel struct {
	model.BaseModel `json:"-"`
	Id              int    `gorm:"id" json:"id"`
	UserId          int    `gorm:"user_id" json:"userId"`
	NewsStatus      int    `gorm:"news_status" json:"newsStatus"`
	IsTop           int    `gorm:"is_top" json:"isTop"`
	Recommended     int    `gorm:"recommended" json:"recommended"`
	IsSlide         int    `gorm:"is_slide" json:"isSlide"`
	NewsHits        int    `gorm:"news_hits" json:"newsHits"`
	CreateTime      int    `gorm:"create_time" json:"createTime"`
	UpdateTime      int    `gorm:"update_time" json:"updateTime"`
	PublishedTime   int    `gorm:"published_time" json:"publishedTime"`
	DeleteTime      int    `gorm:"delete_time" json:"deleteTime"`
	NewsTitle       string `gorm:"news_title" json:"newsTitle"`
	NewsKeywords    string `gorm:"news_keywords" json:"newsKeywords"`
	NewsExcerpt     string `gorm:"news_excerpt" json:"newsExcerpt"`
	NewsSource      string `gorm:"news_source" json:"newsSource"`
	Thumbnail       string `gorm:"thumbnail" json:"thumbnail"`
	IsJump          int    `gorm:"is_jump" json:"isJump"`
	JumpUrl         string `gorm:"jump_url" json:"jump_url"`
}

func NewNewsModelFactory(sqlType string) *NewModel {
	return &NewModel{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (n *NewModel) TableName() string {
	return "cms_news"
}
func (n *NewModel) GetListNews(param *cmsmenu_params.QueryNewsParams, m map[string]interface{}) (total, page int, list []*NewModel, err error) {
	// 获取所有的栏目
	menuList, err := n.GetMenuList()
	if err != nil {
		return
	}
	if len(param.CateId) > 0 {
		// 查询可发布栏目 id
		menuListSlice := convert.SliceMaps(menuList)
		cateIds := param.CateId
		for _, cid := range cateIds {
			sonList := slicetree.FindSonByParentId(menuListSlice, int64(cid), "parentid", "id")
			for _, v := range sonList {
				if v["catetype"] == 2 {
					param.CateId = append(param.CateId, int(convert.Int64(v["id"])))
				}
			}
		}
	}
	db := n.DB.Table("cms_news as news")
	if param != nil {
		if len(param.CateId) > 0 {
			db = db.Joins("inner join cms_category_news as cate", "cate.news_id=news.id").Where("cate.category_id in(?)", param.CateId).Find(&list)
			db = db.Group("cate.news_id").Find(&list)
		}
		if param.KeyWords != "" {
			db = db.Where("news.news_title like ?", "%"+param.KeyWords+"%").Find(&list)
		}
	}
	if len(m) > 0 {
		db = db.Where(m).Find(&list)
	}
	db = db.Joins("left join tb_user user","news.user_id=user.id")
	total = len(list)
	if param.PageSize == 0 {
		param.PageSize = 10
	}
	if param.PageNum == 0 {
		param.PageNum = 1
	}
	if param.PageNum > 0 && param.PageSize > 0 {
		res := (param.PageNum - 1) * param.PageSize
		db.Limit(param.PageSize).Offset(res).Order("news.id desc").Find(&list)
	}
	return total, param.PageNum, list, nil
}
func (n *NewModel) GetMenuList() (list []*cmsmenu_model.CmsMenuModel, err error) {
	sql := "select * from sys_category"
	db := n.DB.Raw(sql).Find(&list)
	if db.RowsAffected > 0 {
		return list, nil
	} else {
		return nil, nil
	}
}
