package dict_model

import (
	"errors"
	"fmt"
	"yooome/model"
	"yooome/model/sys_dict_data"
	"yooome/model/sys_dict_type"
	"yooome/utils/convert"
	"yooome/utils/ytime"
)

type Dict struct {
	model.BaseModel `json:"-"`
	DictId          uint64 `gorm:"dict_id" json:"dict_id"`
	DictName        string `gorm:"dict_name" json:"dict_name"`
	DictType        string `gorm:"dict_type" json:"dict_type"`
	Status          uint   `gorm:"status" json:"status"`
	CreateBy        uint64 `gorm:"create_by" json:"create_by"`
	CreateTime      uint64 `gorm:"create_time" json:"create_time"`
	UpdateBy        uint   `gorm:"update_by" json:"update_by"`
	UpdateTime      uint   `gorm:"update_time" json:"update_time"`
	Remark          string `gorm:"remark" json:"remark"`
}

// 创建 dict model 实体工厂
func NewDicModelFactory(sqlType string) *Dict {
	return &Dict{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (d *Dict) TableName() string {
	return "sys_dict_type"
}

func (d *Dict) GetDictListByPage(dict *sys_dict_type.PageReqParams) (total, page int, list []*Dict, err error) {
	db := d.DB.Find(&list)
	if dict != nil {
		if dict.DictName != "" {
			// sql := "select * from sys_dict_type where dict_name like ?"
			// db = db.Raw(sql, "%"+dict.DictName+"%").Find(&list)
			db = db.Where("dict_name like ?", "%"+dict.DictName+"%").Find(&list)

		}
		if dict.DictType != "" {
			// sql := "select * from sys_dict_type where dict_type like ?"
			db = db.Where("dict_type like ?", "%"+dict.DictType+"%").Find(&list)
		}
		if dict.Status != "" {
			// sql := "select * from sys_dict_type where status=?"
			// db = db.Raw(sql, convert.Int64(dict.Status)).Find(&list)
			db = db.Where("status = ?", convert.Int64(dict.Status)).Find(&list)
		}
		if dict.BeginTime != "" {
			// sql := "select * from sys_dict_type where create_time >= ?"
			t, _ := ytime.StrToTime(dict.BeginTime)
			// db = db.Raw(sql, t.Timestamp()).Find(&list)
			db = db.Where("create_time >= ?", t.Timestamp()).Find(&list)
		}
		if dict.EndTime != "" {
			// sql := "select * from sys_dict_type where end_time <= ?"
			t, _ := ytime.StrToTime(dict.EndTime)
			// db = db.Raw(sql, t.Timestamp()).Find(list)
			db = db.Where("end_time <= ?", t.Timestamp()).Find(&list)
		}
	}
	total = len(list)
	if dict.PageNum == 0 {
		dict.PageNum = 1
	}
	if dict.PageNum > 0 && dict.PageSize > 0 {
		res := (dict.PageNum - 1) * dict.PageSize
		db.Limit(dict.PageSize).Offset(res).Order("dict_id asc").Find(&list)
	}
	return total, dict.PageNum, list, err
}

func (d *Dict) GetStatusByDictType(dictType, defaultValue, emptyLabel string) (map[string]interface{}, error) {
	var dict *Dict
	if dictType != "" {
		d.DB.Where(map[string]interface{}{"dict_type": dictType, "status": 1}).First(&dict)
	}
	var dicData []*sys_dict_data.DictData
	if dict != nil {
		dicData = sys_dict_data.NewDictDataFactory("").GetStatusByDictType(dictType)
	}
	values := make([]map[string]interface{}, len(dicData))
	for k, v := range dicData {
		isDefault := 0
		if defaultValue != "" {
			if defaultValue == v.DictValue {
				isDefault = 1
			}
		} else if emptyLabel == "" {
			isDefault = v.IsDefault
		}
		values[k] = map[string]interface{}{
			"key":       v.DictValue,
			"value":     v.DictLabel,
			"isDefault": isDefault,
			"remark":    v.Remark,
		}
	}
	if emptyLabel != "" {
		res := map[string]interface{}{
			"isDefault": 0,
			"key":       "",
			"value":     emptyLabel,
		}
		values = append(values, res)
	}
	result := map[string]interface{}{
		"dict_name": dict.DictName,
		"remark":    dict.Remark,
		"values":    values,
	}
	return result, nil
}
func (d *Dict) AddDict(addDictType *sys_dict_type.AddDictType, userId int64) (result int64, err error) {

	dictName := addDictType.DictName
	remark := addDictType.Remark
	dictType := addDictType.DictType
	status := addDictType.Status
	createTime := convert.Uint64(ytime.Timestamp())
	createBy := convert.Uint64(userId)
	// 添加 字典类型
	sql := "insert into sys_dict_type(dict_name,dict_type,remark,status,create_time,create_by) values (?,?,?,?,?,?)"
	res := d.DB.Exec(sql, dictName, dictType, remark, status, createTime, createBy)
	fmt.Println("res = ", res.RowsAffected)
	if res.RowsAffected > 0 {
		return res.RowsAffected, nil
	} else {
		return res.RowsAffected, errors.New("add dict type error")
	}
}

func (d *Dict) GetDetail(dictId string) (*Dict, error) {
	var dict *Dict
	id := convert.Int64(dictId)
	sql := "select * from sys_dict_type where dict_id = ?"
	res := d.DB.Exec(sql, id).Find(&dict)
	if res.RowsAffected > 0 {
		return dict, nil
	} else {
		return nil, errors.New("can not find  sys dict type id  ")
	}
}

func (d *Dict) UpdateDict(updateDict *sys_dict_type.UpdateDictType, userId int64) (result int64, err error) {
	dictId := updateDict.DictId
	dictName := updateDict.DictName
	remark := updateDict.Remark
	dictType := updateDict.DictType
	status := updateDict.Status
	updateTime := convert.Uint64(ytime.Timestamp())
	updateBy := convert.Uint64(userId)
	sql := "update sys_dict_type set dict_name = ?, dict_type = ?, remark = ?, status = ?, update_time = ?, update_by = ? where dict_id = ?"
	res := d.DB.Exec(sql, dictName, dictType, remark, status, updateTime, updateBy, dictId)
	if res.RowsAffected > 0 {
		return res.RowsAffected, nil
	} else {
		return res.RowsAffected, errors.New("add dict type error")
	}
}

// 删除dict
func (d *Dict) DeleteByDictIds(dictIds []int) bool {
	sql := "delete from sys_dict_type where dict_id in (?)"
	res := d.DB.Exec(sql, dictIds)
	if res.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
