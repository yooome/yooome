package sys_dept_model

import (
	"errors"
	"time"
	"yooome/model"
	"yooome/model/sys_dept_params"
	"yooome/utils/convert"
)

// dept 数据列表数据
type Dept struct {
	model.BaseModel `json:"-"`
	DeptID          uint                   `gorm:"dept_id" json:"deptId"`
	ParentID        int                    `gorm:"parentId" json:"parentId"`
	Ancestors       string                 `gorm:"ancestors" json:"ancestors"`
	DeptName        string                 `gorm:"dept_name" json:"deptName"`
	OrderNum        int                    `gorm:"order_num" json:"orderNum"`
	Leader          string                 `gorm:"leader" json:"leader"`
	Phone           string                 `gorm:"phone" json:"phone"`
	Email           string                 `gorm:"email" json:"email"`
	Status          string                 `gorm:"status" json:"status"`
	DelFlag         string                 `gorm:"del_flag" json:"delFlag"`
	CreateBy        string                 `gorm:"create_by" json:"createBy"`
	CreateTime      *time.Time             `gorm:"create_time" json:"createTime"`
	UpdateBy        string                 `gorm:"update_by" json:"updateBy"`
	UpdateTime      *time.Time             `gorm:"update_time" json:"updateTime"`
	SearchValue     interface{}            `gorm:"-" json:"searchValue"`
	Remark          string                 `gorm:"-" json:"remark"`
	DataScope       interface{}            `gorm:"-" json:"dataScope"`
	Params          map[string]interface{} `gorm:"-" json:"params"`
	ParentName      string                 `gorm:"-" json:"parentName"`
	Children        []interface{}          `gorm:"-" json:"children"`
}

// 创建 dict model 实体工厂
func NewDeptModelFactory(sqlType string) *Dept {
	return &Dept{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (d *Dept) TableName() string {
	return "sys_dept"
}

// 获取部门列表
func (d *Dept) GetList() (list []*Dept, err error) {
	sql := "select * from sys_dept where status = ?"
	db := d.DB.Exec(sql, "1").Find(&list)
	//status := "1"
	//db := d.DB.Where("status = ?", status).Find(&list)
	if db.RowsAffected == 0 {
		return nil, errors.New("")
	} else {
		for _, v := range list {
			if v.Children == nil {
				v.Children = []interface{}{}
			}
		}
		return list, nil
	}
}

// 根据参数获取部门列表
func (d *Dept) GetDeptList(deptParams *sys_dept_params.DeptParams) (list []*Dept, err error) {
	db := d.DB.Find(&list)
	if deptParams.DeptName != "" {
		db.Where("dept_name like ?", "%"+deptParams.DeptName+"%")
	}
	if deptParams.Status != "" {
		db.Where("status like ?", deptParams.Status)
	}
	for _, v := range list {
		if v.Children == nil {
			v.Children = []interface{}{}
		}
	}
	return list, nil
}

// 部门添加
func (d *Dept) AddDept(addDeptParams *sys_dept_params.AddDeptParams) bool {
	parentId := addDeptParams.ParentID
	deptName := addDeptParams.DeptName
	orderNum := addDeptParams.OrderNum
	leader := addDeptParams.Leader
	phone := addDeptParams.Phone
	email := addDeptParams.Email
	status := addDeptParams.Status
	createBy := addDeptParams.CreateBy
	createTime := time.Now()

	sql := "insert into sys_dept (parent_id,dept_name,order_num,leader,phone,email,status,create_by,create_time)" +
		"values (?,?,?,?,?,?,?,?,?)"
	db := d.DB.Exec(sql, parentId, deptName, orderNum, leader, phone, email, status, createBy, createTime)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}

// 更新部门信息
func (d *Dept) UpdateDept(updateDeptParams *sys_dept_params.UpdateDeptParams) bool {
	deptId := updateDeptParams.DeptId
	parentId := updateDeptParams.ParentID
	deptName := updateDeptParams.DeptName
	orderNum := updateDeptParams.OrderNum
	leader := updateDeptParams.Leader
	phone := updateDeptParams.Phone
	email := updateDeptParams.Email
	status := updateDeptParams.Status
	updateBy := updateDeptParams.UpdateBy
	updateTime := time.Now()
	sql := "update sys_dept set parent_id = ?,dept_name = ?,order_num = ?,leader = ?,phone = ?,email = ?,status = ?,update_by = ?,update_time = ? where dept_id = ?"
	db := d.DB.Exec(sql, parentId, deptName, orderNum, leader, phone, email, status, updateBy, updateTime, deptId)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (d *Dept) GetDeptById(id string) (dept *Dept) {
	db := d.DB.Where("dept_id = ?", id).Find(&dept)
	if db.RowsAffected > 0 {
		return dept
	} else {
		return nil
	}
}
func (d *Dept) ExcludeById(id string) (dept []*Dept) {
	db := d.DB.Where("dept_id not in (?)", id).Find(&dept)
	for _, v := range dept {
		if v.Children == nil {
			v.Children = []interface{}{}
		}
	}
	if db.RowsAffected > 0 {
		return dept
	} else {
		return nil
	}
}
func (d *Dept) DeleteDept(id string) bool {
	sql := "delete from sys_dept where dept_id = ?"
	db := d.DB.Exec(sql, convert.Int64(id))
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
