package job_model

import (
	"errors"
	"yooome/model"
	"yooome/model/job_params"
	"yooome/utils/convert"
	"yooome/utils/ytime"
)

type Job struct {
	model.BaseModel `json:"-"`
	JobId           int64  `gorm:"job_id" json:"jobId"`
	JobName         string `gorm:"job_name" json:"jobName"`
	JobParams       string `gorm:"job_params" json:"jobParams"`
	JobGroup        string `gorm:"job_Group" json:"jobGroup"`
	InvokeTarget    string `gorm:"invoke_target" json:"invokeTarget"`
	CronExpression  string `gorm:"cron_expression" json:"cronExpression"`
	MisfirePolicy   int64  `gorm:"misfire_policy" json:"misfirePolicy"`
	ConCurrent      int64  `gorm:"concurrent" json:"conCurrent"`
	Status          int64  `gorm:"status" json:"status"`
	CreateBy        int64  `gorm:"create_by" json:"create_by"`
	CreateTime      int64  `gorm:"create_time" json:"createTime"`
	UpdateBy        int64  `gorm:"update_by" json:"updateBy"`
	UpdateTime      int64  `gorm:"update_time" json:"updateTime"`
	Remark          string `gorm:"remark" json:"remark"`
}

func NewJobFactory(sqlType string) *Job {
	return &Job{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (j *Job) TableName() string {
	return "sys_job"
}
func (j *Job) GetJobList(params *job_params.QueryJobParams) (total, page int, list []*Job, err error) {
	db := j.DB.Find(&list)
	if params.Status != "" {
		db = db.Where("status = ?", convert.Int64(params.Status)).Find(&list)
	}
	if params.JobGroup != "" {
		db = db.Where("job_group = ?", convert.Int64(params.JobGroup)).Find(&list)
	}
	if params.JobName != "" {
		db = db.Where("job_name like ?", "%"+params.JobName+"%").Find(&list)
	}
	total = len(list)
	if params.PageNum == 0 {
		params.PageNum = 1
	}
	if params.PageSize == 0 {
		params.PageSize = 10
	}
	if params.PageNum > 0 && params.PageSize > 0 {
		res := (params.PageNum - 1) * params.PageSize
		db.Limit(params.PageSize).Offset(res).Order("dict_id asc").Find(&list)
	}
	return total, params.PageNum, list, nil
}
func (j *Job) AddJob(params *job_params.AddJobParams, userId int64) bool {
	jobName := params.JobName
	jobGroup := params.JobGroup
	invoke := params.InvokeTarget
	jobParams := params.JobParams
	cronexpression := params.CronExpression
	misfire := params.MisfirePolicy
	remark := params.Remark
	status := params.Status
	createTime := ytime.Now().Timestamp()
	createBy := userId
	sql := "insert into sys_job (job_name,job_group,invoke_target,job_params,cron_expression,misfire_policy,remark,status,create_time,create_by)" +
		" values (?,?,?,?,?,?,?,?,?,?)"
	db := j.DB.Exec(sql, jobName, jobGroup, invoke, jobParams, cronexpression, misfire, remark, status, createTime, createBy)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (j *Job) GetJobById(id string) (job *Job, err error) {
	sql := "select * from sys_job where job_id =?"
	db := j.DB.Raw(sql, convert.Int64(id)).Find(&job)
	if db.RowsAffected > 0 {
		return job, nil
	} else {
		return nil, errors.New("get job by id error")
	}
}
func (j *Job) UpdateJob(params *job_params.UpdateJobParams, userId int) bool {
	jobName := params.JobName
	jobGroup := params.JobGroup
	invoke := params.InvokeTarget
	jobParams := params.JobParams
	cronexpression := params.CronExpression
	misfire := params.MisfirePolicy
	remark := params.Remark
	status := params.Status
	updateTime := ytime.Now().Timestamp()
	updateBy := userId
	jobId := params.JobId

	sql := "update sys_job set job_name = ?,job_group = ?,invoke_target = ?,job_params = ?,cron_expression = ?,misfire_policy = ?,remark = ?,status = ?,update_time = ?,update_by = ?" +
		" where job_id = ?"
	db := j.DB.Exec(sql, jobName, jobGroup, invoke, jobParams, cronexpression, misfire, remark, status, updateTime, updateBy, jobId)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
func (j *Job) DelJobById(id string) bool {
	sql := "delete from sys_job where job_id =?"
	db := j.DB.Exec(sql, int(convert.Int64(id)))
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
