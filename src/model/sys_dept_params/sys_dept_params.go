package sys_dept_params

// 部门搜索参数
type DeptParams struct {
	DeptName string `form:"deptName" json:"deptName"`
	Status   string `form:"status" json:"status"`
}
type AddDeptParams struct {
	ParentID int    `form:"parentId" json:"parentId"`
	DeptName string `form:"deptName" json:"deptName"`
	OrderNum int    `form:"orderNum" json:"orderNum"`
	Leader   string `form:"leader" json:"leader"`
	Phone    string `form:"phone" json:"phone"`
	Email    string `form:"email" json:"email"`
	Status   string `form:"status" json:"status"`
	CreateBy string `form:"-" json:"-"`
}
type UpdateDeptParams struct {
	DeptId   int    `form:"deptId" json:"deptId"`
	ParentID int    `form:"parentId" json:"parentId"`
	DeptName string `form:"deptName" json:"deptName"`
	OrderNum int    `form:"orderNum" json:"orderNum"`
	Leader   string `form:"leader" json:"leader"`
	Phone    string `form:"phone" json:"phone"`
	Email    string `form:"email" json:"email"`
	Status   string `form:"status" json:"status"`
	UpdateBy string `form:"-" json:"-"`
}
