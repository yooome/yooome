package role_params

import "yooome/model"

type RoleParams struct {
	RoleId    uint   `json:"roleId" form:"roleId"`
	DataScope string `json:"dataScope" form:"dataScope"`
	DeptIds   []uint `json:"deptIds" form:"deptIds"`
}

type RoleDeptParams struct {
	model.BaseModel `json:"-"`
	RoleId          uint `json:"roleId" form:"roleId" gorm:"role_id"`
	DeptId          uint `json:"deptIds" form:"deptIds" gor:"dept_id"`
}
