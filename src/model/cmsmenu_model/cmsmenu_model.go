package cmsmenu_model

import (
	"errors"
	"yooome/model"
	"yooome/model/cmsmenu_params"
	"yooome/utils/convert"
)

type CmsMenuModel struct {
	model.BaseModel `json:"-"`
	Id              int     `gorm:"id" json:"id"`
	ParentId        int     `gorm:"parent_id" json:"parentId"`
	ModelId         int     `gorm:"model_id" json:"modelId"`
	Status          int     `gorm:"status" json:"status"`
	DeleteTime      int     `gorm:"delete_time" json:"deleteTime"`
	ListOrder       float64 `gorm:"list_order" json:"listOrder"`
	Name            string  `gorm:"name" json:"name"`
	Alias           string  `gorm:"alias" json:"alias"`
	Description     string  `gorm:"description" json:"description"`
	SeoTitle        string  `gorm:"seo_title" json:"seoTitle"`
	SeoKeywords     string  `gorm:"seo_keywords" json:"seoKeywords"`
	SeoDescription  string  `gorm:"seo_description" json:"seo_description"`
	ListTpl         string  `gorm:"list_tpl" json:"listTpl"`
	OneTpl          string  `gorm:"one_tpl" json:"oneTpl"`
	More            string  `gorm:"more" json:"more"`
	CateType        int     `gorm:"cate_type" json:"cateType"`
	CateAddress     string  `gorm:"cate_address" json:"cateAddress"`
	CateContent     string  `gorm:"cate_content" json:"cate_content"`
	ListTemplate    string  `gorm:"list_template" json:"listTemplate"`
	ContentTemplate string  `gorm:"content_template" json:"contentTemplate"`
}

func NewCmsMenuModelFactory(sqlType string) *CmsMenuModel {
	return &CmsMenuModel{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (c *CmsMenuModel) TableName() string {
	return "cms_category"
}
func (c *CmsMenuModel) GetCmsMenuList(params *cmsmenu_params.CmsMenuParams) (list []*CmsMenuModel, err error) {
	db := c.DB.Find(&list)
	if params.Name != "" {
		db = db.Where("name like ?", "%"+params.Name+"%").Find(&list)
	}
	if params.Status != "" {
		db = db.Where("status = ?", convert.Int64(params.Status)).Find(&list)
	}
	return list, nil
}
func (c *CmsMenuModel) GetTreeSelect() (list []*CmsMenuModel, err error) {
	db := c.DB.Find(&list).Order("list_order asc,id asc")
	if db.RowsAffected > 0 {
		return list, nil
	} else {
		return nil, errors.New("get data error")
	}
}
