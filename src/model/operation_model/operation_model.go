package operation_model

import (
	"yooome/model"
	"yooome/model/operation_params"
	"yooome/utils/ytime"
)

type OperationModel struct {
	model.BaseModel `json:"-"`
	OperId          int    `gorm:"oper_id" json:"operId"`
	Title           string `gorm:"title" json:"title"`
	BussinessType   int    `gorm:"bussiness_type" json:"bussinessType"`
	Method          string `gorm:"method" json:"method"`
	RequestMethod   string `gorm:"request_method" json:"requestMethod"`
	OperatorType    int    `gorm:"operator_type" json:"operatorType"`
	OperName        string `gorm:"oper_name" json:"operName"`
	DeptName        string `gorm:"dept_name" json:"deptName"`
	OperUrl         string `gorm:"oper_url" json:"operUrl"`
	OperIp          string `gorm:"oper_ip" json:"operIp"`
	OperLocation    string `gorm:"oper_location" json:"operLocation"`
	OperParam       string `gorm:"oper_param" json:"operParam"`
	JsonResult      string `gorm:"json_result" json:"jsonResult"`
	Status          int    `gorm:"status" json:"status"`
	ErrorMsg        string `gorm:"error_msg" json:"errorMsg"`
	OperTime        int64  `gorm:"oper_time" json:"operTime"`
}

func NewOperationModelFactory(sqlType string) *OperationModel {
	return &OperationModel{BaseModel: model.BaseModel{DB: model.DBConn(sqlType)}}
}
func (o *OperationModel) TableName() string {
	return "sys_oper_log"
}
func (o *OperationModel) GetOperationLogList(params *operation_params.OperationParams) (total, page int, list []*OperationModel, err error) {
	order := "oper_id desc"
	if params.PageNum == 0 {
		params.PageNum = 1
	}
	if params.PageSize == 0 {
		params.PageSize = 10
	}
	db := o.DB.Find(&list)
	if params.OperName != "" {
		db = db.Where("oper_name like ?", "%"+params.OperName+"%")
	}
	if params.Title != "" {

		db = db.Where("title like ?", "%"+params.Title+"%")
	}
	if params.BeginTime != "" {
		tm, _ := ytime.StrToTime(params.BeginTime)
		db.Where("oper_time >= ?", tm.Timestamp())
	}
	if params.EndTime != "" {
		tm, _ := ytime.StrToTime(params.EndTime)
		db.Where("oper_time <= ?", tm.Timestamp())
	}

	if params.SortName != "" {
		if params.SortOrder != "" {
			order = params.SortName + " " + params.SortOrder
		} else {
			order = params.SortName + " DESC"
		}
	}
	total = len(list)
	if params.PageNum > 0 && params.PageSize > 0 {
		res := (params.PageNum - 1) * params.PageSize
		db.Limit(params.PageSize).Offset(res).Order(order).Find(&list)
	}
	return total, params.PageNum, list, nil
}
func (o *OperationModel) DelOperationLog(ids []int) bool {
	sql := "delete from sys_oper_log where oper_id in (?)"
	db := o.DB.Exec(sql, ids)
	if db.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}
