package req_role_params

// 前台分页请求
type ReqRoleParams struct {
	RoleName  string `json:"roleName" form:"roleName"`   // 参数名称
	BeginTime string `json:"beginTime" form:"beginTime"` // 开始时间
	EndTime   string `json:"endTime" form:"endTime"`     // 结束时间
	Status    string `json:"status" form:"status"`       // 状态
	PageNum   int    `json:"pageNum" form:"pageNum"`     // 当前页码
	PageSize  int    `json:"pageSize" form:"pageSize"`   // 每页数
}
type AddRoleParams struct {
	Name      string `json:"roleName" form:"roleName"`
	Status    string `json:"status" form:"status"`
	ListOrder int    `json:"roleSort" form:"roleSort"`
	Remark    string `json:"remark" form:"remark"`
}

// 更新角色
type UpdateRoleParams struct {
	Id        int    `json:"roleId" form:"roleId"`
	Name      string `json:"roleName" form:"roleName"`
	Status    string `json:"status" form:"status"`
	ListOrder int    `json:"roleSort" form:"roleSort"`
	Remark    string `json:"remark" form:"remark"`
}
