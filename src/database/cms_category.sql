-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: gfast
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cms_category`
--

DROP TABLE IF EXISTS `cms_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '分类父id',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态,1:发布,0:不发布',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `list_order` float NOT NULL DEFAULT '10000' COMMENT '排序',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `alias` varchar(255) NOT NULL DEFAULT '' COMMENT '栏目别名',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '分类描述',
  `seo_title` varchar(100) NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  `list_tpl` varchar(50) NOT NULL DEFAULT '' COMMENT '分类列表模板',
  `one_tpl` varchar(50) NOT NULL DEFAULT '' COMMENT '分类文章页模板',
  `more` text COMMENT '扩展属性',
  `cate_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '分类类型',
  `cate_address` varchar(255) NOT NULL DEFAULT '' COMMENT '跳转地址',
  `cate_content` text COMMENT '单页内容',
  `list_template` varchar(150) DEFAULT NULL COMMENT '列表页模板',
  `content_template` varchar(150) DEFAULT NULL COMMENT '内容页模板',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_category`
--

LOCK TABLES `cms_category` WRITE;
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
INSERT INTO `cms_category` VALUES (16,0,9,1,0,0,'最新推荐','','','','','','','','',3,'/cms','','list/list.html','content/show.html'),(17,0,9,1,0,0,'新闻快报','','','','','','','','',1,'','','list/list.html','content/show.html'),(18,0,9,1,0,0,'科技创新','','','','','','','','',1,'','','list/list.html','content/show.html'),(19,0,9,1,0,0,'房产家居','','','','','','','','',1,'','','list/list.html','content/show.html'),(20,0,9,1,0,0,'汽车资讯','','','','','','','','',1,'','','list/list.html','content/show.html'),(21,0,9,1,0,0,'旅游攻略','','','','','','','','',2,'','','list/list.html','content/show.html'),(22,0,9,1,0,0,'体育视界','','','','','','','','',2,'','','list/list.html','content/show.html'),(23,0,9,1,0,0,'财经股票','','','','','','','','',2,'','','list/list.html','content/show.html'),(24,0,9,1,0,0,'美食天地','','','','','','','','',2,'','','list/list.html','content/show.html'),(25,0,9,1,0,0,'教育公益','','','','','','','','',2,'','','list/list.html','content/show.html'),(26,17,9,1,0,0,'国内新闻','','','','','','','','',1,'','','list/list.html','content/show.html'),(27,17,9,1,0,0,'国际新闻','','','','','','','','',1,'','','list/list.html','content/show.html'),(28,26,9,1,0,0,'时政新闻','','','','','','','','',2,'','','list/list.html','content/show.html'),(29,26,9,1,0,0,'趣文概要','','','','','','','','',2,'','','list/list.html','content/show.html'),(30,18,9,1,0,0,'创业俱乐部','','','','','','','','',2,'','','list/list.html','content/show.html'),(31,18,9,1,0,0,'区块链','','','','','','','','',2,'','','list/list.html','content/show.html'),(32,18,9,1,0,0,'互联网','','','','','','','','',2,'','','list/list.html','content/show.html'),(33,19,9,1,0,0,'新房','','','','','','','','',2,'','','list/list.html','content/show.html'),(34,19,9,1,0,0,'二手房','','','','','','','','',1,'','','list/list.html','content/show.html'),(35,20,9,1,0,0,'上市新车','','','','','','','','',2,'','','list/list.html','content/show.html'),(36,20,9,1,0,0,'用车小百科','','','','','','','','',2,'','','list/list.html','content/show.html'),(37,27,9,1,0,0,'国际时政','','','','','','','','',2,'','','list/list.html','content/show.html'),(38,27,9,1,0,0,'人文风景','','','','','','','','',2,'','','list/list.html','content/show.html');
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-14 19:49:11
