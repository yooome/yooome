-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: gfast
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cms_news`
--

DROP TABLE IF EXISTS `cms_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cms_news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '发表者用户id',
  `news_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:已发布;0:未发布;',
  `is_top` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否置顶;1:置顶;0:不置顶',
  `recommended` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否推荐;1:推荐;0:不推荐',
  `is_slide` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否幻灯 1是 0否',
  `news_hits` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '查看数',
  `news_like` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `published_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `news_title` varchar(150) NOT NULL DEFAULT '' COMMENT 'post标题',
  `news_keywords` varchar(150) NOT NULL DEFAULT '' COMMENT 'seo keywords',
  `news_excerpt` varchar(500) NOT NULL DEFAULT '' COMMENT 'post摘要',
  `news_source` varchar(150) NOT NULL DEFAULT '' COMMENT '转载文章的来源',
  `thumbnail` text COMMENT '缩略图',
  `is_jump` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否跳转地址',
  `jump_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL COMMENT '跳转地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT COMMENT='cms信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_news`
--

LOCK TABLES `cms_news` WRITE;
/*!40000 ALTER TABLE `cms_news` DISABLE KEYS */;
INSERT INTO `cms_news` VALUES (111,2,1,1,1,1,22,0,1603786563,1603960985,1603786513,0,' 炒面多做这一步会特香，吃一碗根本不够','','炒面是我家的日常早餐之一，也没有为什么，主要是比较耐饿，记得少时妈妈在炒面的时候，总放点咱仨姐弟喜欢的配菜，虽然说是普普通通的，但不知为啥特别香，有时想来，也不失为一种让人怀念惦记的家常美味，也可以说是家的味道','','pub_upload/2020-10-27/c6niwywhjvjcjgde1s.jpeg',0,''),(113,2,1,1,1,1,3,0,1603875574,1603960967,1603875523,0,'夏天这样做馒头，比酵母馒头省事又好吃','','饮食常常能体现一个地方的特色。北方夏天吃馒头是常事，早上一碗稀饭几个馒头，搭配适量的咸菜是北方人常吃的早餐，这样简易又营养的早餐，足以体现北方人对面食的热爱，而山西人吃面食也是全国有名的，而这边人吃馒头大都是老面发酵的方式，老面馒头吃起来劲道、麦香、柔软、又白又大，吃过的人都知道这样的老式发酵的面食才地道好吃！','','pub_upload/2020-10-28/c6oegkcicda8ecbjfl.jpg',0,''),(115,2,1,1,1,1,13,0,1603937779,1604026571,1603937711,0,'超美的三亚,5天4晚全五星酒店高大上自由行','','海南，简称“琼”，中华人民共和国省级行政区，省会海口。海南省位于中国最南端，北以琼州海峡与广东省划界，西临北部湾与广西壮族自治区和越南相对，东濒南海与台湾省对望， 东南和南边在南海中与菲律宾、文莱和马来西亚为邻。省内的三沙市是中国地理位置最南、总面积最大（含海域面积）、陆地面积最小且人口最少的地级区划。海南省下辖4个地级市，5个县级市，4个县，6个自治县；截至2017年末，全省常住人口925.76万人。','','pub_upload/2020-10-29/c6p0haeubqqsnlhejw.jpg',0,''),(116,2,1,0,1,1,1,0,1603961187,1603961253,1603961115,0,'菜市场的这4种猪肉，再便宜也不能买，主妇必看','','现如今，越来越多的小伙伴加入到无肉不欢的队伍中来，可见肉是多么勾人胃口。今天发哥要说的，是最常见的猪肉，菜场的猪肉五花八门，怎么挑选才最好呢？值得注意的是，下面这4种猪肉，再便宜也不能买。','','pub_upload/2020-10-29/c6p8s7uup7x4xymfea.jpg',0,''),(118,2,1,0,1,1,3,0,1603961393,1603961393,1603961317,0,'家常豆腐这样做，非常下饭，简单易学','','来到青海出差，吃到了好多当地的美食啊，味道正宗不说，服务还很好。其中有这样一道菜，是由肉丸、豆腐和肉块炖成的，味道很小，这边的朋友说这是当地最有特色的食物之一。遗憾的是，我实在是没有记住是什么菜名，所以大概只能吃到这一次了，一想到这里，我就觉得莫名的有些难过。','','pub_upload/2020-10-29/c6p8uohyl7kg6caoxf.jpg',0,''),(119,2,1,0,1,0,2,0,1603961644,1603961653,1603961440,0,'人莫不能饮食也，鲜能知味也','','时下，流行一种说法：懂得生活的人，是不会亏待自己的胃的；热爱生活的人，是不会让自己的心灵寂寞的。因为人最原始的需求就是由温饱而求舌尖美味，继而寻找精神上的愉悦。品味美食，总能让人得到心理上的满足，是一种让自己快乐的生活方式，是真实自然的慰藉；是一种向生命表达善意的举动，是水到渠成的升华。','','pub_upload/2020-10-29/c6p8xz30dbhcxzygv6.png',0,''),(120,2,1,0,1,1,1,0,1603961750,1604020305,1603961685,0,'超好吃的成都美食，这是一篇有味道的文章','','中国地广物博，各地饮食更是五花八门，各具持色...其中就有这样一座城市，让你来了就不想走...不是因为太美的风景驻足，而是它牢牢地拴住了一个吃货的心，如果说，在这么多城市中选择一座一定要去的城市，那肯定是吃货的天堂“天府之国”的成都啦~下面就来简单的盘点一下，成都必吃的美食吧','','pub_upload/2020-10-29/c6p8za2ovxxgp3vejs.jpg',0,''),(121,2,1,0,0,0,3,0,1604020316,1604020440,1604020308,0,'14部门联合发文 19条措施力挺扩内需促消费','','10月29日，国家发展改革委发布通知指出，国家发展改革委等14个部门共同研究制定了《近期扩内需促消费的工作方案》（以下简称《方案》）。《方案》从推动线下服务消费加速“触网”、开辟服务消费新模式、实施促进实物消费政策、加大对制造业企业支持力度等四个方面推出19条措施。','','',0,''),(122,2,1,0,1,0,0,0,1604028550,1604028655,1604028490,0,'特朗普发推提主张：美国和欧盟同时取消所有关税','','特朗普在推文中称，“欧盟明天将前往华盛顿商讨贸易协议。我有个主张，美国和欧盟同时取消所有关税、壁垒以及补贴。那样最终会被视为自由市场和公平贸易！希望他们能做到，我们是已经做好了准备——但他们不会。”','','pub_upload/2020-10-30/c6pwno8goau0ukjg3q.jpg',0,''),(123,2,1,0,0,0,1,0,1604028769,1604028769,1604028668,0,'徐峥最满意的影片,《药神》火爆背后的主创故事','','2017年3月，南京，深夜，驻组宾馆，《我不是药神》的主要演员，徐峥、周一围、王传君、谭卓、章宇、杨新鸣、李乃文围读剧本以后，开始一场场地排戏。像在话剧舞台上，演员们都十分投入，随着语气时而高昂，时而低沉，幽默、温馨、凝重等气氛在简单的小会议室里轮番上演。','','pub_upload/2020-10-30/c6pwqm121lnoncnkvs.jpg',0,''),(124,2,1,0,1,1,1,0,1604028843,1604028862,1604028801,0,'最全汇总：iPhone X Plus的传闻都在这儿了','','外界普遍预计，苹果将在两个月以后发布三款新 iPhone，包括 5.8 英寸 iPhone X 的继任者、尺寸更大的 iPhone X Plus 以及一款价格较低的 LCD iPhone 机型。我们暂且将较大英寸的 OLED 机型称为 iPhone X Plus，到目前为止，关于这款机型的传闻有很多，今天我们就来汇总一下。','','pub_upload/2020-10-30/c6pwrxducjwol7kb2q.jpeg',0,''),(125,2,1,0,1,0,2,0,1604028929,1604029262,1604028885,0,'百城住宅库存连跌35个月 三四线库存创10年新低','','随着三、四线城市楼市交易量的提升，全国百城住宅库存再次降低。截至6月底，全国百城住宅库存已经连续35个月减少。','','pub_upload/2020-10-30/c6pwsqcu6mi85nxf0g.png',0,''),(126,2,1,0,1,0,0,0,1604029060,1604029060,1604028980,0,'特斯拉Model 3高清官图公布:设计极简 配15英寸大号iPad','','特斯拉Model 3是是特斯拉Model X系列的新品，北京时间2016年4月1日11点30分在美国发布，基础售价35000美元。在开放官网预定之前，仅靠门店排队预定，Model 3的订单数量已经超过11.5万辆。','','pub_upload/2020-10-30/c6pwuapknpacaljqm5.jpg',0,''),(127,2,1,0,1,1,3,0,1604029132,1604029132,1604029092,0,'世界杯最大牌女球迷，一举动令人钦佩','','要说世界杯最大牌的女球迷，一定就是克罗地亚总统格拉巴尔·基塔罗维奇，为了支持球队，她没有像其他国家元首那样坐在VIP包厢，而是和球迷出现在看台上，这样的亲民让人钦佩。如今，克罗地亚杀入8强，接下来四分之一决赛对阵俄罗斯，格拉巴尔·基塔罗维奇也将赴现场为球队加油打气。','','pub_upload/2020-10-30/c6pwvdddc9kolqpyvu.jpg',0,''),(128,2,1,0,0,0,0,0,1604029202,1604041385,1604029162,0,'百胜中国“捐一元”开启第十一年爱心之旅','','对于贵州省三都水族自治县都江镇甲找村甲找小学的师生而言，7月24日是个美好的日子。学校里来了许多和善的叔叔阿姨，他们带着满满的爱心，为孩子们送上用心准备的礼物：精美的图书、崭新的体育用品……中国扶贫基金会执行副理事长王行最','','pub_upload/2020-10-30/c6pww9dkku60m8yrpv.jpg',0,''),(129,2,1,0,0,1,1,0,1604029355,1604029380,1604029313,0,'摩洛哥撒哈拉攻略干货，解锁网红拍照点','','撒哈拉之于我，除了三毛还有地理书上学到的世界上最大的沙漠。我想亲近沙子，想去看看这个世界上最大的猫砂盆到底长啥样。\n进撒哈拉有三种方式。一种是自驾，但是有些路段不建议自驾，且摩洛哥警察真的很坑啊啊，随便就是几百上千罚款不见了。第二种是三天两夜散拼团，一般十几个人以上，可以结识世界各地的朋友，但是拼团的住宿条件和车况不太好，听说晚上都是睡大通铺，而且不能洗澡充电。第三种是三天两夜私人团，沙漠里有独立房间，可以洗澡且是白帐篷，好看的多且是4驱越野车。私人团还有四天三夜，行程会更放松，有时间的强烈建议4天3夜。','','pub_upload/2020-10-30/c6pwy5wn1398e93sjy.jpg',0,''),(130,2,1,0,0,0,4,0,1604029504,1605769566,1604029434,0,'时政新闻眼丨五中全会公报这些新提法，即将走进你我生活','','“全面促进消费”“扎实推动共同富裕”“新型工农城乡关系”“实施乡村建设行动”“促进经济社会发展全面绿色转型”“确保二〇二七年实现建军百年奋斗目标”……这些新提法、新部署体现了鲜明的问题导向，彰显了以人民为中心的发展思想。','','pub_upload/2020-10-30/c6pwzxde4qkkciir2g.jpeg',0,'');
/*!40000 ALTER TABLE `cms_news` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-17 11:35:49
