-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: gfast
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `menu_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型 0目录 1菜单 2按钮',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `always_show` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '显示状态',
  `path` varchar(100) NOT NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(100) NOT NULL DEFAULT '' COMMENT '组件路径',
  `is_frame` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否外链 1是 0否',
  `module_type` varchar(30) NOT NULL DEFAULT '' COMMENT '所属模块',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE,
  KEY `weigh` (`weigh`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='菜单节点表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` VALUES (1,0,'system/config','系统配置','system','','Admin tips',0,1497429920,1497430320,0,1,1,'config','',0,'sys_admin',0),(2,0,'system/auth','权限管理','peoples','','',0,1497429920,1592380524,0,1,1,'system/auth','',0,'sys_admin',0),(3,0,'system/monitor','系统监控','monitor','','',0,1497429920,1592380560,0,1,1,'monitor','',0,'sys_admin',0),(4,0,'system/cms','cms管理','form','','',0,1497429920,1592380573,0,1,1,'cms','',0,'sys_admin',0),(5,1,'system/config/dict/list','字典典管理','dict','','',1,1497429920,1617477920,0,1,1,'dict/list','system/config/dict/list',0,'sys_admin',0),(6,1,'system/config/params/list','参数管理','date-range','','',1,1497429920,1592380510,0,1,1,'params/list','system/config/params/list',0,'sys_admin',0),(8,2,'system/auth/menuList','菜单管理','nested','','',1,1497429920,1592380548,0,1,1,'menuList','system/auth/menuList',0,'sys_admin',0),(21,5,'system/config/dict/delete','删除字典','','','',2,1592363629,1592363629,0,1,1,'','',0,'sys_admin',0),(16,8,'system/auth/addMenu','添加菜单','','','',2,1592357208,1600396699,0,1,0,'','',0,'sys_admin',0),(17,8,'system/auth/editMenu','修改菜单','','','',2,1592357231,1592357274,0,1,1,'','',0,'sys_admin',0),(18,8,'system/auth/deleteMenu','删除菜单','','','',2,1592357268,1592357268,0,1,1,'','',0,'sys_admin',0),(19,5,'system/config/dict/add','添加字典','','','',2,1592363585,1592363585,0,1,1,'','',0,'sys_admin',0),(20,5,'system/config/dict/edit','修改字典','','','',2,1592363604,1592363604,0,1,1,'','',0,'sys_admin',0),(22,5,'system/config/dict/dataList','字典数据管理','','','',2,1592363790,1592365455,0,1,1,'dataList','',0,'sys_admin',0),(23,5,'system/config/dict/dataAdd','添加字典数据','','','',2,1592380398,1593411393,0,1,1,'','',0,'sys_admin',0),(24,5,'system/config/dict/dataEdit','修改字典数据','','','',2,1592380417,1593411398,0,1,1,'','',0,'sys_admin',0),(25,5,'system/config/dict/dataDelete','删除字典数据','','','',2,1592380449,1593411403,0,1,1,'','',0,'sys_admin',0),(26,2,'system/auth/roleList','角色管理','logininfor','','',1,1592385260,1592385291,0,1,1,'role','system/auth/roleList',0,'sys_admin',0),(27,26,'system/auth/addRole','添加角色','','','',2,1592389821,1592389821,0,1,1,'','',0,'sys_admin',0),(28,2,'system/auth/dept','部门管理','peoples','','',1,1592449845,1592449845,0,1,1,'dept','system/auth/dept',0,'sys_admin',0),(29,26,'system/auth/editRole','修改角色','','','',2,1592469153,1592469153,0,1,1,'','',0,'sys_admin',0),(30,26,'system/auth/statusSetRole','设置角色状态','','','',2,1592469201,1592469201,0,1,1,'','',0,'sys_admin',0),(31,26,'system/auth/deleteRole','删除角色','','','',2,1592469234,1592469234,0,1,1,'','',0,'sys_admin',0),(32,4,'system/cms/menu/list','栏目管理','tree-table','','',1,1592469318,1592469318,0,1,1,'menu','system/cms/menu/list',0,'sys_admin',0),(33,2,'system/auth/post','岗位管理','tab','','',1,1592558968,1592558968,0,1,1,'post','system/auth/post',0,'sys_admin',0),(34,3,'system/monitor/online/list','在线用户','cascader','','',1,1593328511,1593328511,0,1,1,'online','system/monitor/online/list',0,'sys_admin',0),(35,4,'system/cms/news/list','文章管理','log','','',1,1593331969,1593331991,0,1,1,'news','system/cms/news/list',0,'sys_admin',0),(36,3,'system/monitor/job','定时任务','clipboard','','',1,1593332305,1593332434,0,1,1,'job','system/monitor/job',0,'sys_admin',0),(37,3,'system/monitor/server','服务监控','dict','','',1,1593419394,1593419394,0,1,1,'server','system/monitor/server',0,'sys_admin',0),(38,3,'system/monitor/logininfor','登录日志','chart','','',1,1593423378,1593423505,0,1,1,'logininfor','system/monitor/logininfor',0,'sys_admin',0),(39,3,'system/monitor/operlog','操作日志','dashboard','','',1,1593485097,1593485097,0,1,1,'operlog','system/monitor/operlog',0,'sys_admin',0),(40,2,'system/auth/userList','用户管理','user','','',1,1593572523,1593572523,0,1,1,'user','system/auth/userList',0,'sys_admin',0),(41,6,'system/config/params/add','添加参数','','','',2,1593684331,1593684331,0,1,1,'','',0,'sys_admin',0),(42,6,'system/config/params/edit','修改参数','','','',2,1593684351,1593684351,0,1,1,'','',0,'sys_admin',0),(43,6,'system/config/params/delete','删除参数','','','',2,1593684470,1593684470,0,1,1,'','',0,'sys_admin',0),(44,28,'system/dept/addDept','添加部门','','','',2,1593738070,1593738070,0,1,1,'','',0,'sys_admin',0),(45,28,'system/dept/editDept','修改部门','','','',2,1593738097,1593738097,0,1,1,'','',0,'sys_admin',0),(46,28,'system/dept/delDept','删除部门','','','',2,1593738125,1593738125,0,1,1,'','',0,'sys_admin',0),(47,33,'system/post/add','添加岗位','','','',2,1593738444,1593738444,0,1,1,'','',0,'sys_admin',0),(48,33,'system/post/edit','修改岗位','','','',2,1593738567,1593738567,0,1,1,'','',0,'sys_admin',0),(49,33,'system/post/delete','删除岗位','','','',2,1593738590,1593738590,0,1,1,'','',0,'sys_admin',0),(50,40,'system/auth/addUser','添加用户','','','',2,1593738798,1593738798,0,1,1,'','',0,'sys_admin',0),(51,40,'system/auth/editUser','修改用户','','','',2,1593738950,1593738950,0,1,1,'','',0,'sys_admin',0),(52,40,'system/auth/resetUserPwd','密码重置','','','',2,1593739001,1593739001,0,1,1,'','',0,'sys_admin',0),(53,40,'system/auth/changeUserStatus','状态设置','','','',2,1593739079,1593739079,0,1,1,'','',0,'sys_admin',0),(54,40,'system/auth/deleteAdmin','删除用户','','','',2,1593739113,1593739113,0,1,1,'','',0,'sys_admin',0),(55,34,'system/monitor/online/forceLogout','强制退出','','','',2,1593739201,1593739201,0,1,1,'','',0,'sys_admin',0),(56,36,'system/monitor/job/add','添加任务','','','',2,1593740041,1593740041,0,1,1,'','',0,'sys_admin',0),(57,36,'system/monitor/job/edit','修改任务','','','',2,1593740062,1593740062,0,1,1,'','',0,'sys_admin',0),(58,36,'system/monitor/job/start','开启任务','','','',2,1593740105,1593740105,0,1,1,'','',0,'sys_admin',0),(59,36,'system/monitor/job/stop','停止任务','','','',2,1593740139,1593740139,0,1,1,'','',0,'sys_admin',0),(60,36,'system/monitor/job/delete','删除任务','','','',2,1593740165,1593740165,0,1,1,'','',0,'sys_admin',0),(61,38,'system/monitor/loginlog/delete','删除','','','',2,1593740342,1593740342,0,1,1,'','',0,'sys_admin',0),(62,38,'system/monitor/loginlog/clear','清空','','','',2,1593740359,1593740359,0,1,1,'','',0,'sys_admin',0),(63,39,'system/monitor/operlog/delete','删除','','','',2,1593740422,1593740422,0,1,1,'','',0,'sys_admin',0),(64,39,'system/monitor/operlog/clear','清空','','','',2,1593740434,1593740434,0,1,1,'','',0,'sys_admin',0),(65,32,'system/cms/menu/add','添加栏目','','','',2,1593740504,1593740504,0,1,1,'','',0,'sys_admin',0),(66,32,'system/cms/menu/edit','修改栏目','','','',2,1593740521,1593740521,0,1,1,'','',0,'sys_admin',0),(67,32,'system/cms/menu/sort','栏目排序','','','',2,1593740549,1593740549,0,1,1,'','',0,'sys_admin',0),(68,32,'system/cms/menu/delete','删除栏目','','','',2,1593740568,1593740568,0,1,1,'','',0,'sys_admin',0),(69,35,'system/cms/news/add','添加文章','','','',2,1593740691,1593740691,0,1,1,'','',0,'sys_admin',0),(70,35,'system/cms/news/edit','修改文章','','','',2,1593740711,1593740711,0,1,1,'','',0,'sys_admin',0),(71,35,'system/cms/news/delete','删除文章','','','',2,1593740730,1593740730,0,1,1,'','',0,'sys_admin',0),(72,0,'system/model','模型管理','table','','',0,1593742999,1593742999,0,1,1,'model','',0,'sys_admin',0),(73,72,'system/model/category/list','模型分类','tree-table','','',1,1593743065,1593743065,0,1,1,'category','system/model/category/list',0,'sys_admin',0),(74,72,'system/model/info/list','模型列表','list','','',1,1593743131,1594781057,0,1,1,'list','system/model/info/list',0,'sys_admin',0),(75,0,'system/tools','系统工具','server','','',0,1594016328,1594016328,0,1,1,'system/tools','',0,'sys_admin',0),(76,75,'system/tools/build','表单构建','build','','',1,1594016392,1594016808,0,1,1,'build','system/tools/build',0,'sys_admin',0),(77,75,'system/tools/gen','代码生成','code','','',1,1594016637,1594016637,0,1,1,'gen','system/tools/gen',0,'sys_admin',0),(78,0,'system/plug','扩展管理','logininfor','','',0,1594169636,1605862005,0,1,1,'system/plug','',1,'sys_admin',0),(79,78,'system/plug/ad','广告管理','color','','',0,1594169691,1595410089,0,1,1,'adManage','',0,'sys_admin',0),(80,79,'system/plug/ad/type/list','广告位管理','nested','','',1,1594169783,1595405904,0,1,1,'adtype','system/plug/ad/type/list',0,'sys_admin',0),(81,79,'system/plug/ad/info/list','广告列表','list','','',1,1594169949,1596418803,0,1,1,'adlist','system/plug/ad/info/list',0,'sys_admin',0),(82,75,'system/tools/api','系统接口','guide','','',1,1594951684,1594951684,0,1,1,'api','system/tools/api',0,'sys_admin',0),(83,78,'system/plug/link','友情链接','cascader','','',0,1595381634,1595403122,0,1,1,'link','',0,'sys_admin',0),(84,83,'system/plug/link/type/list','分类管理','component','','',1,1595381717,1595381717,0,1,1,'type','system/plug/link/type/list',0,'sys_admin',0),(85,83,'system/plug/link/info/list','链接管理','list','','',1,1595381754,1595381754,0,1,1,'info','system/plug/link/info/list',0,'sys_admin',0),(86,1,'system/config/webSet','站点设置','system','','',1,1596420340,1596420340,0,1,1,'/webSet','system/config/webSet',0,'sys_admin',0),(106,4,'cms','cms前端展示','education','','',1,1604040725,1604040798,0,1,1,'http://localhost:8200/cms','system/cms/news/list',1,'',0),(107,0,'system/wf','流程管理','cascader','','',0,1606967359,1606967359,0,1,1,'system/wf','',0,'',0),(108,107,'system/wf/flow/list','工作流列表','component','','',1,1606967458,1606967516,0,1,1,'flow/list','system/wf/flow/list',0,'',0),(109,108,'system/wf/flow/design','设计流程','','','',2,1606967544,1607074912,0,1,1,'','',0,'',0),(110,107,'system/wf/flow/news','测试业务','job','','',1,1606967599,1606967599,0,1,1,'flow/news','system/wf/flow/news',0,'',0),(111,107,'system/wf/flow/monitoring','流程监控','eye-open','','',1,1606967670,1606967680,0,1,1,'monitoring','system/wf/flow/monitoring',0,'',0),(112,0,'system/plugin/blog','简单博客管理','education','','',0,1607400193,1607400193,0,1,1,'blog','',0,'',0),(113,112,'system/plugin/blog/log/list','日志管理','log','','',1,1607400265,1607400265,0,1,1,'log','plugin/blog/log/list',0,'',0),(114,112,'system/plugin/blog/classification/list','分类管理','tab','','',1,1607400305,1607400305,0,1,1,'classification','plugin/blog/classification/list',0,'',0),(115,112,'system/plugin/blog/comment/list','评论管理','date','','',1,1607400346,1607400346,0,1,1,'comment','plugin/blog/comment/list',0,'',0),(116,112,'plugin/blog','博客前端展示','eye-open','','',1,1607400394,1607400394,0,1,1,'http://localhost:8200/plugin/blog','plugin/blog/log/list',1,'',0);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-24 17:15:10
