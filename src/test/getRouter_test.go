package test

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
	"testing"
	"yooome/global/variable"
	"yooome/routers"
	"yooome/utils/gorminit"
	"yooome/utils/yml_config"
)

// 测试 GetRouter 接口
func TestGetRouterDemo(T *testing.T) {
	// 1.检查配置文件是否存在
	if _, err := os.Stat(variable.BasePath + "/config/gorm.yml"); err != nil {
		fmt.Println("配置文件gorm.yml不存在....")
	}
	variable.ConfigYml = yml_config.NewYamlFileFactory()
	variable.ConfigYml.ConfigFileChangeListen()
	variable.ConfigGormYml = variable.ConfigYml.Clone("gorm")
	variable.ConfigGormYml.ConfigFileChangeListen()
	dbMysql, _ := gorminit.InitMysqlConnect()
	if dbMysql == nil {
		fmt.Println(" gormInit dbMysql ....", dbMysql)
		return
	}
	fmt.Println("连接成功！！！", dbMysql)
	router := gin.Default()
	routers.Routers(router)
	// 修改添加 router 的端口号
	_ = router.Run(":8080")
}
