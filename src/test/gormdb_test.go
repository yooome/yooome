package test

import (
	"fmt"
	"os"
	"testing"
	"yooome/global/variable"
	"yooome/model/role"
	"yooome/utils/gorminit"
	"yooome/utils/snow_flake"
	"yooome/utils/yml_config"
)

func TestGormDB(T *testing.T) {
	fmt.Println("init mysql connect ....")
	gorminit.InitMysqlConnect()
	fmt.Println("end mysql connect ...")
}
func TestGormPath(T *testing.T) {
	fmt.Println("123.......")
	fileInfo, err := os.Stat(variable.BasePath + "/config/gorm.yml")
	if err != nil {
		fmt.Println("gorm.yml file is not find")
	}
	fmt.Println(" fineInfo name = ", fileInfo.Name())
}
func TestGormConnect(T *testing.T) {
	// 1.检查配置文件是否存在
	if _, err := os.Stat(variable.BasePath + "/config/gorm.yml"); err != nil {
		fmt.Println("配置文件gorm.yml不存在....")
	}
	variable.ConfigYml = yml_config.NewYamlFileFactory()
	variable.ConfigYml.ConfigFileChangeListen()
	variable.ConfigGormYml = variable.ConfigYml.Clone("gorm")
	variable.ConfigGormYml.ConfigFileChangeListen()
	dbMysql, _ := gorminit.InitMysqlConnect()
	if dbMysql == nil {
		fmt.Println(" gormInit dbMysql ....", dbMysql)
		return
	}
	fmt.Println("连接成功！！！", dbMysql)
	variable.SnowFlake = snow_flake.NewSnowFlakeFactory()

	snowID := variable.SnowFlake.GetID()
	fmt.Println("snow flake snowId = ", snowID)
	snowID = variable.SnowFlake.GetID()
	fmt.Println("snow flake snowId = ", snowID)
}
func TestRoleList(T *testing.T) {
	// 1.检查配置文件是否存在
	if _, err := os.Stat(variable.BasePath + "/config/gorm.yml"); err != nil {
		fmt.Println("配置文件gorm.yml不存在....")
	}
	variable.ConfigYml = yml_config.NewYamlFileFactory()
	variable.ConfigYml.ConfigFileChangeListen()
	variable.ConfigGormYml = variable.ConfigYml.Clone("gorm")
	variable.ConfigGormYml.ConfigFileChangeListen()
	dbMysql, _ := gorminit.InitMysqlConnect()
	variable.GormDBMysql = dbMysql
	fmt.Println("dbMysql = ", dbMysql)

	// 获取 role 角色信息
	 role.NewRoleFactory("").GetRoleList()
}
