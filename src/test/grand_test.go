package test

import (
	"fmt"
	"testing"
)

func TestGrandDemo(T *testing.T) {
	str := Letters(10)
	fmt.Println("letters result = ", str)
	bufferChan = make(chan []byte, gBUFFER_SIZE)
}
func TestBufferChan(T *testing.T) {
	i := 0
	b := make([]byte, 300)
	var SIZE = 1000
	bufferChan = make(chan []byte, SIZE)
	for {
		copy(b[i:], <-bufferChan)
		i += 5
		if i >= 300 {
			break
		}
	}
	fmt.Println("bufferChan = ", &bufferChan)
	fmt.Println("b = ", b)
	fmt.Println("b length = ", len(b))
}
func TestChannelDemo(T *testing.T) {
	var intChan chan int
	intChan = make(chan int, 10000)
	intChan <- 10
	intChan <- 20
	intChan <- 30
	fmt.Println("length = ",len(intChan))
	fmt.Printf("intChan 的值 = %v intChan 本身的地址= %p", <-intChan, &intChan)
}
