package test

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/mojocn/base64Captcha"
	"github.com/sirupsen/logrus"
	"log"
	"math"
	"net"
	"os"
	"regexp"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"sync/atomic"
	"testing"
	"time"
	model "yooome/model/users"
	"yooome/service"
	"yooome/utils/tools"
	isempty "yooome/utils/utilsisempty"
	"yooome/utils/ytime"
)

func TestDemo(t *testing.T) {
	logrus.WithFields(logrus.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)
	// 设置日志等级
	fmt.Println("设置日志等级")
	logrus.SetLevel(logrus.WarnLevel)
	id, bas64 := service.GetCaptchaDriverString()
	fmt.Println("id = ", id, "bas64 = ", bas64)
}
func TestDemo2(T *testing.T) {
	id := "Mqrh2efs0r66Qa91edjN"
	bas64 := "r6u2"
	boo := service.VerifyCaptchaString(id, bas64)
	fmt.Println("测试的验证码 result = ", boo)
}

func TestDemo3(T *testing.T) {
	id, b64s := GetCaptcha()
	fmt.Println("id = ", id, "bas64 = ", b64s)
	// 中间可以发给客户端，让他输入然后你来验证
	boo := Verify(id, b64s)
	fmt.Println("测试的验证码 result = ", boo)
}

//  获取验证码
func GetCaptcha() (string, string) {
	// 生成默认数字
	driver := base64Captcha.DefaultDriverDigit
	// 生成base64图片
	c := base64Captcha.NewCaptcha(driver, store)

	// 获取
	id, b64s, err := c.Generate()
	if err != nil {
		fmt.Println("Register GetCaptchaPhoto get base64Captcha has err:", err)
		return "", ""
	}
	return id, b64s
}

// 验证验证码
func Verify(id string, val string) bool {
	if id == "" || val == "" {
		return false
	}
	// 同时在内存清理掉这个图片
	return store.Verify(id, val, true)
}

func TestNew(T *testing.T) {
	var a int
	fmt.Println(&a)
	var p *int
	p = &a
	*p = 20
	fmt.Println(a)
}
func TestJson(T *testing.T) {
	var data = []byte(`{"status": 200}`)
	res := make(map[string]interface{})
	res["zhang"] = "zhang"
	dur := time.Duration(2)
	time.Sleep(dur)
	var result map[string]interface{}

	if err := json.Unmarshal(data, &result); err != nil {
		log.Fatalln(err)
	}
	fmt.Println(result)
}
func TestMap(T *testing.T) {
	map1 := make(map[int]int, 10)
	map1[5] = 50
	map1[8] = 80
	map1[6] = 60
	map1[1] = 10
	map1[2] = 20
	map1[3] = 30
	map1[4] = 40
	fmt.Println(map1)
	var keys []int
	for idx, _ := range map1 {
		keys = append(keys, idx)
	}
	// 排序
	sort.Ints(keys)
	fmt.Println(keys)
	for _, val := range keys {
		fmt.Printf("map1[%v]=%v \n", val, map1[val])
	}
}
func TestTCpServer(T *testing.T) {
	fmt.Println("服务器开始监听...")
	listen, err := net.Listen("tcp", "0.0.0.0:8888")
	if err != nil {
		fmt.Println("listen err = ", err)
		return
	}
	defer listen.Close() // 延时关闭 listen
	// for循环等待客户端的连接
	for {
		fmt.Println("等待客户端的连接")
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("listen accept err = ", err)
		} else {
			fmt.Printf("accept suc con = %v 客户端 ip = %v\n", conn, conn.RemoteAddr().String())
		}
		// 这里准备一个协程，为客户端服务
		go process(conn)
	}
}

func process(conn net.Conn) {
	defer conn.Close() // 关闭conn
	for {
		// 创建一个新的切片
		buf := make([]byte, 1024)
		// 1、等待客户端通过conn发送消息
		// 2、如果客户端没有 write[发送],那么协程就会阻塞在这里
		fmt.Printf("服务端等待客户端%s 发送信息\n", conn.RemoteAddr().String())
		// 3、从conn中读取消息
		n, err := conn.Read(buf)
		if err != nil {
			fmt.Printf("客户端退出 err = %v", err)
			return
		}
		// 4、显示客户端发送的内容到服务器的终端
		fmt.Printf(string(buf[:n]))
		fmt.Println()
	}
}
func TestTCpClient(T *testing.T) {
	// net.Dail 进行服务端的连接
	conn, err := net.Dial("tcp", "127.0.0.1:8888")
	if err != nil {
		fmt.Println("client dial err = ", err)
		return
	}
	// 1、客户端发送单行数据，然后就退出
	reader := bufio.NewReader(os.Stdin)
	// 2、从终端读取一行用户输入，并准备发送给客户端
	line, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("readString err = ", err)
	}
	n, err := conn.Write([]byte(line))
	if err != nil {
		fmt.Println("conn write err = ", err)
	}
	fmt.Printf("客户端发送了 %d 字节数据，并退出", n)
}
func TestGetNumCpu(T *testing.T) {
	num := runtime.NumCPU()
	runtime.GOMAXPROCS(num)
	fmt.Println("num = ", num)
}
func TestComputed(T *testing.T) {
	var (
		result = make(map[int]int, 10)
	)
	for i := 1; i <= 200; i++ {
		go test(i, result)
	}
	time.Sleep(time.Second * 10)
	for i, v := range result {
		fmt.Printf("map[%d]=%d\n", i, v)
	}
}

func test(n int, result map[int]int) {
	res := 1
	for i := 1; i <= n; i++ {
		res *= i
	}
	result[n] = res
}
func TestChannel(T *testing.T) {
	intChan := make(chan int, 3)
	intChan <- 100
	intChan <- 200
	close(intChan)
	// 当通道关闭后，读取数据才是可以的
	n1 := <-intChan
	fmt.Println("n1 = ", n1)
}
func TestChannelPrime(T *testing.T) {
	intChan := make(chan int, 1000)
	primeChan := make(chan int, 2000) // 放入结果
	// 标识退出的管道
	exitChan := make(chan bool, 4)
	// 开启一个协程，向intChan放入1-8000个数
	go putNum(intChan)
	// 开启4个协程，从intChan取出数据，并判断是否为素数，如果是，就放入primeChan
	for i := 0; i < 4; i++ {
		go primeNum(intChan, primeChan, exitChan)
	}
	go func() {
		for i := 0; i < 4; i++ {
			<-exitChan
		}
		// 当我们从exitChan 中取出4个结果，就可以放心的关闭 primeChan
		close(primeChan)
	}()
	// 遍历我们的primeChan，把结果取出来
	for {
		res, ok := <-primeChan
		if !ok {
			break
		}
		// 将结果输出
		fmt.Printf("素数=%d\n", res)
	}
	fmt.Println("main线程退出")
}

func primeNum(intChan chan int, primeChan chan int, exitChan chan bool) {
	var flag bool
	for {
		time.Sleep(time.Millisecond * 10)
		num, ok := <-intChan
		if !ok {
			break
		}
		flag = true
		// 判断 num 是不是素数
		for i := 2; i < num; i++ {
			if num%i == 0 {
				flag = false
				break
			}
		}
		if flag {
			// 将这个数就放到primeChan中
			primeChan <- num
		}
		fmt.Println("有一个primeNum协程因为取不到数据退出")
		exitChan <- true

	}
}

func putNum(intChan chan int) {
	for i := 1; i <= 8000; i++ {
		intChan <- i
	}
	// 关闭
	close(intChan)
}
func TestChannelDemo2(T *testing.T) {
	intChan := make(chan int, 1000)
	for i := 0; i < 1000; i++ {
		intChan <- i
	}
	close(intChan)
	for i := 0; i < 10; i++ {
		fmt.Println("intChan 线程 = ", <-intChan)
	}
	fmt.Println("main线程", len(intChan))
}
func TestSelect(T *testing.T) {
	// 使用select可以解决管道取数阻塞的问题
	// 1、定义一个管道10个int数据
	intChan := make(chan int, 10)
	for i := 0; i < 10; i++ {
		intChan <- i
	}
	// 2、定义一个管道5个数据string
	strChan := make(chan string, 5)
	for i := 0; i < 5; i++ {
		strChan <- "hello" + fmt.Sprintf("%d", i)
	}
	// 传统的方法在遍历管道的时候不关闭，如果不关闭会阻塞导致deadlock
	// 而在实际的开发过程中我们不好确定什么时候关闭
	// 可以使用 select  方式可以确定
	for {
		select {
		case val := <-intChan:
			fmt.Printf("从intChan中读取数据%d\n", val)
			time.Sleep(time.Second)
		case str := <-strChan:
			fmt.Printf("从strChan中读取数据%s\n", str)
			time.Sleep(time.Second)
		default:
			fmt.Printf("都取不到了.....")
			time.Sleep(time.Second)
			return
		}
	}
}
func TestRecover(T *testing.T) {
	go sayHello()
	go hello()
	for i := 0; i < 10; i++ {
		fmt.Println("main ok = ", i)
		time.Sleep(time.Second)
	}
}

func hello() {
	// 这里可以使用 defer + recover 捕获异常
	defer func() {
		// 捕获 hello 抛出的异常
		if err := recover(); err != nil {
			fmt.Println(" hello() 发生错误", err)
		}
	}()
	// 定义一个 map
	var myMap map[int]string
	myMap[0] = "hello"
}

func sayHello() {
	for i := 0; i < 10; i++ {
		time.Sleep(time.Second)
		fmt.Println("hello,world")
	}
}
func TestSwapInt(T *testing.T) {
	var value int32
	value = 42
	val := atomic.SwapInt32(&value, 0)
	fmt.Println("value = ", val)
}
func TestMath(T *testing.T) {
	fmt.Println("math.MaxInt64 = ", math.MaxInt64)
	fmt.Println("math.MaxInt32 = ", math.MaxInt32)
	num := math.MaxInt64 / 1000000
	fmt.Println(num)
}
func TestTime(T *testing.T) {
	t := time.Now()
	year := t.Year()
	month := t.Month()
	day := t.Day()
	fmt.Println(year, month, day)

}
func TestParseInLocation(T *testing.T) {
	t, _ := time.ParseInLocation("2006-01-02 15:04:05", "2019-01-08 15:22:22", time.Local)

	fmt.Println("in location time t.Unix = ", t)
	t2, _ := time.Parse("2006-01-02 15:04:05", "2019-01-08 15:22:22")
	fmt.Println("parse time t2 = ", t2)
	fmt.Println("parse time t2.Unix = ", t2.Unix())
}

// golang中time包：时间字符串和时间戳的互相替换
func TestTimeTemplate(T *testing.T) {
	// 外部传入的时间戳（秒为单位），必须为int64类型
	t := int64(1546926630)
	// 外部传入的时间字符串
	t1 := "2019-01-08 13:50:30"
	// 时间转换的模板，golang中这能是 "2016-01-02 15:04:05" 【go的诞生时间】
	timeTemplate1 := "2006-01-02 15:04:05"
	timeTemplate2 := "2006/01/02 15:04:05"
	timeTemplate3 := "2006-01-02"
	timeTemplate4 := "15:04:05"
	// 将时间戳格式化日期字符串
	fmt.Println("timeTemplate1 = ", time.Unix(t, 0).Format(timeTemplate1))
	fmt.Println("timeTemplate2 = ", time.Unix(t, 0).Format(timeTemplate2))
	fmt.Println("timeTemplate3 = ", time.Unix(t, 0).Format(timeTemplate3))
	fmt.Println("timeTemplate4 = ", time.Unix(t, 0).Format(timeTemplate4))

	// 将时间字符串转化为时间戳
	stamp, _ := time.ParseInLocation(timeTemplate1, t1, time.Local)
	fmt.Println("stamp = ", stamp)
}

// 获取每天的零点时间戳，一个小时的时间戳是3600
func TestTimeParse(T *testing.T) {
	timeStr := time.Now().Format("2006-01-02")
	fmt.Println("timeStr = ", timeStr)
	stamp, _ := time.ParseInLocation("2006-01-02", timeStr, time.Local)
	fmt.Println("timeUnix = ", stamp.Unix())
}

// time包的中的 UTC 和 UnixData
func TestUTCAndUnixData(T *testing.T) {
	// 获取当前时间
	t := time.Now()
	fmt.Println("t = ", t)
	fmt.Println("t.UTC = ", t.UTC().Format(time.UnixDate))
	fmt.Println("t unix = ", t.Unix())
	timestamp := strconv.FormatInt(t.UTC().UnixNano(), 10)
	fmt.Println("timestamp = ", timestamp)
	timestamp = timestamp[:10]
	fmt.Println(timestamp)
}
func TestParseDemo(T *testing.T) {
	const longForm = "Jan 2, 2006 at 3:04pm (MST)"
	t, _ := time.Parse(longForm, "Jun 21, 2017 at 0:00am (PST)")
	fmt.Println("t = ", t)

	const shortForm = "2006-Jan-02"
	t1, _ := time.Parse(shortForm, "2017-Jun-21")
	fmt.Println("t1 = ", t1)

	t2, _ := time.Parse("01/02/2006", "06/21/2017")
	fmt.Println("t2 = ", t2)
	fmt.Println(t2.Unix())

	res, err := strconv.ParseInt("1498003200", 10, 64)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(res, 0)
	fmt.Println("tm = ", tm)
	var stamp int64 = 1498003200
	stampRes := time.Unix(stamp, 0)
	fmt.Println(stampRes.Format("2006-01-02 03:04:05 PM"))
	fmt.Println(stampRes.Format("02/01/2006 15:04:05 PM"))
}
func TestTimeDemo(T *testing.T) {
	// Add 时间相加
	now := time.Now()
	// ParseDuration parses a duration string.
	// A duration string is a possibly signed sequence of decimal numbers,
	// each with optional fraction and a unit suffix,
	// such as "300ms", "-1.5h" or "2h45m".
	//  Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
	// 10分钟前
	m, _ := time.ParseDuration("-2m")
	m1 := now.Add(m)
	fmt.Println(m1)

	// 8个小时前
	h, _ := time.ParseDuration("-1h")
	h1 := now.Add(8 * h)
	fmt.Println(h1)

	// 一天前
	d, _ := time.ParseDuration("-24h")
	d1 := now.Add(d)
	fmt.Println(d1)

	printSplit(50)

	// 10分钟后
	mm, _ := time.ParseDuration("1m")
	mm1 := now.Add(mm)
	fmt.Println(mm1)

	// 8小时后
	hh, _ := time.ParseDuration("1h")
	hh1 := now.Add(hh)
	fmt.Println(hh1)

	// 一天后
	dd, _ := time.ParseDuration("24h")
	dd1 := now.Add(dd)
	fmt.Println(dd1)

	printSplit(50)

	// Sub 计算两个时间差
	subM := now.Sub(m1)
	fmt.Println(subM.Minutes(), "分钟")

	sumH := now.Sub(h1)
	fmt.Println(sumH.Hours(), "小时")

	sumD := now.Sub(d1)
	fmt.Printf("%v 天\n", sumD.Hours()/24)

}
func printSplit(count int) {
	fmt.Println(strings.Repeat("#", count))
}
func TestLocationZone(T *testing.T) {
	location, err := time.LoadLocation("America/Los_Angeles")
	fmt.Println("err = ", err)
	if err != nil {
		return
	}
	fmt.Println("location = ", location)
	err = os.Setenv("TZ", location.String())
	if err != nil {
		fmt.Println("err = ", err)
	}
}
func TestUnixNao(T *testing.T) {
	t := time.Now()
	fmt.Println("unixNano = ", t.UnixNano()/1e9)
	fmt.Println("unix = ", t.Unix())
}
func TestMonth(T *testing.T) {
	t := time.Now()
	fmt.Println("time month = ", int(t.Month()))
}
func TestGre(T *testing.T) {
	timeRegexPattern1 := `(\d{4}[-/\.]\d{2}[-/\.]\d{2})[:\sT-]*(\d{0,2}:{0,1}\d{0,2}:{0,1}\d{0,2}){0,1}\.{0,1}(\d{0,9})([\sZ]{0,1})([\+-]{0,1})([:\d]*)`
	timeRegex1, _ := regexp.Compile(timeRegexPattern1)
	fmt.Println("timeRegex1 = ", timeRegex1)
}
func TestQuote(T *testing.T) {
	str := strconv.Quote("hello&&lll")
	fmt.Println("str = ", str)
}
func TestYTime(T *testing.T) {
	t := ytime.Now()
	t1 := time.Now()
	fmt.Println("t = ", t)
	fmt.Println("t1 = ", t1)
}
func TestTools(T *testing.T) {
	key := tools.EncryptCBC("1234", "HqmP1KLMuz09Q0Bu")
	fmt.Println("encryptCBC key ", key)
	result := tools.DecryptCBC(key, "HqmP1KLMuz09Q0Bu")
	fmt.Println("result = ", result)
}
func TestIsTemplateIsNull(T *testing.T) {
	var user model.UserModel
	userModel := &model.UserModel{}
	if isempty.IsNil(user) {
		fmt.Println("user  is null ")
	}
	if !isempty.IsNil(userModel) {
		fmt.Println("user model is null ")
		return
	}
	fmt.Println("user model is not null")
}
func TestStrEqualDemo(T *testing.T) {
	str := "yooome"
	res := "YOOOME"
	b := strings.EqualFold(str, res)
	fmt.Println("b = ", b)
}
