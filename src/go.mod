module yooome

go 1.15

require (
	github.com/casbin/casbin/v2 v2.34.0
	github.com/casbin/gorm-adapter/v3 v3.3.2
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gin-gonic/gin v1.7.0
	github.com/go-playground/validator/v10 v10.5.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gomodule/redigo v1.8.4
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mojocn/base64Captcha v1.3.1
	github.com/shirou/gopsutil/v3 v3.22.6
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	github.com/ugorji/go v1.2.5 // indirect
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.9
	gorm.io/plugin/dbresolver v1.1.0
)
