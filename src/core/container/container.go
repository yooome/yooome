package container

import (
	"fmt"
	"sync"
)

// 定义一个容器结构体
type containers struct {
}

// 定义一个全局键值对存储容器
var globalMap sync.Map

// 创建一个容器工厂
func NewContainersFactory() *containers {
	return &containers{}
}

// 1.已键值对的形式将代码注册到容器中
func (c *containers) Set(key string, val interface{}) (res bool) {
	if _, exists := c.KeyIsExists(key); exists == false {
		globalMap.Store(key, val)
		res = true
	} else {
		fmt.Println("未能注册到容器中.....")
	}
	return
}

// 2.判断是否存在已经存在
func (c *containers) KeyIsExists(key string) (interface{}, bool) {
	return globalMap.Load(key)
}
// 3.删除
func (c *containers) Delete(key string)  {
	globalMap.Delete(key)
}
// 4.从缓存中查询
func (c *containers) Get(key string) interface{} {
	if value, exists := c.KeyIsExists(key); exists {
		return value
	}
	return nil
}