package validInterface

import "github.com/gin-gonic/gin"

type ValidatorInterface interface {
	ValidatorParams(context *gin.Context)
}